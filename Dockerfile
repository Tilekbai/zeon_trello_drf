FROM python:3.9.17
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /trello_drf
COPY requirements.txt /trello_drf/
RUN pip install -r requirements.txt
COPY . /trello_drf/