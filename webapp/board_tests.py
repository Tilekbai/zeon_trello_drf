from django.test import (
    TestCase, 
    RequestFactory,
)
from django.core import (
    mail,
    )
from django.urls import (
    reverse,
    )
from .models import (
    Board,
)
from rest_framework import (
    status,
    )
from rest_framework.test import (
    APITestCase,
)
from django.contrib.auth import (
    get_user_model,
    )
from rest_framework.test import (
    force_authenticate,
    )
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from .board_views import *


User = get_user_model()

class BoardListAllTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = BoardListAll.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.boards = Board.objects.create(title='Test board', user=self.user)

    def test_board_list_all_view(self):
        request = self.factory.get('api/')
        force_authenticate(request, user=self.user)
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['boards']), 1)

    def test_board_list_view_empty(self):
        Board.objects.all().delete()
        request = self.factory.get('api/')
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['boards']), 0)

class BoardDetailAPIViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = BoardDetailAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.board = Board.objects.create(
            title='Board 1',
            user=self.user,
        )

    def test_board_detail_view(self):
        request = self.factory.get(f'/api/board_detail/{self.board.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.board.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], self.board.pk)
        self.assertEqual(response.data['title'], self.board.title)
        self.assertEqual(response.data['user'], self.user.id)

    def test_board_detail_view_not_found(self):
        invalid_id = 999
        request = self.factory.get(f'/api/board_detail/{invalid_id}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=invalid_id)
        self.assertEqual(response.status_code, 404)

    def test_board_detail_view_notauthenticated(self):
        request = self.factory.get(f'/api/board_detail/{self.board.pk}/')
        response = self.view(request, pk=self.board.pk)
        self.assertEqual(response.status_code, 403)

class BoardListFavoriteTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = BoardListFavorite.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.boards = Board.objects.create(title='Test board', user=self.user, favorite=True)
        self.another_users_board = Board.objects.create(title='Test board', favorite=True)

    def test_board_list_favourite_view(self):
        request = self.factory.get('api/favourite_boards/')
        force_authenticate(request, user=self.user)
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['boards']), 1)
        self.assertEqual(response.data['boards'][0]['favorite'], True)

    def test_board_list_favourite_view_empty(self):
        Board.objects.all().delete()
        request = self.factory.get('api/favourite_boards/')
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['boards']), 0)

class BoardListArchiveTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = BoardListArchive.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.boards = Board.objects.create(title='Test board', user=self.user, archive=True)
        self.another_users_board = Board.objects.create(title='Test board', archive=True)

    def test_board_list_favourite_view(self):
        request = self.factory.get('api/archive_boards/')
        force_authenticate(request, user=self.user)
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['boards']), 1)
        self.assertEqual(response.data['boards'][0]['archive'], True)

    def test_board_list_favourite_view_empty(self):
        Board.objects.all().delete()
        request = self.factory.get('api/archive_boards/')
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['boards']), 0)

class BoardFavoriteOnTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = BoardFavoriteOnAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.board = Board.objects.create(title='Test board', user=self.user)
        self.another_users_board = Board.objects.create(title='Test board')

    def test_board_favourite_on_view(self):
        request = self.factory.put(f'api/favorite_on/{self.board.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.board.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], self.board.pk)
        self.assertEqual(response.data['favorite'], True)

    def test_board_favourite_on_view_invalid_id(self):
        invalid_pk = 999
        request = self.factory.put(f'api/favorite_on/{invalid_pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=invalid_pk)
        self.assertEqual(response.status_code, 404)

    def test_board_favourite_on_view_another_users_board(self):
        self.another_users_board
        request = self.factory.put(f'api/favorite_on/{self.another_users_board.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.another_users_board.pk)
        self.assertEqual(response.status_code, 403)

    def test_board_favourite_on_view_notauthenticated(self):
        request = self.factory.put(f'api/favorite_on/{self.board.pk}/')
        response = self.view(request)
        self.assertEqual(response.status_code, 403)

class BoardFavoriteOffTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = BoardFavoriteOffAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.board = Board.objects.create(title='Test board', user=self.user, favorite=True)
        self.another_users_board = Board.objects.create(title='Test board')

    def test_board_favourite_off_view(self):
        request = self.factory.put(f'api/favorite_off/{self.board.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.board.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], self.board.pk)
        self.assertEqual(response.data['favorite'], False)

    def test_board_favourite_off_view_invalid_id(self):
        invalid_pk = 999
        request = self.factory.put(f'api/favorite_off/{invalid_pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=invalid_pk)
        self.assertEqual(response.status_code, 404)

    def test_board_favourite_off_view_another_users_board(self):
        self.another_users_board
        request = self.factory.put(f'api/favorite_off/{self.another_users_board.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.another_users_board.pk)
        self.assertEqual(response.status_code, 403)

    def test_board_favourite_off_view_notauthenticated(self):
        request = self.factory.put(f'api/favorite_off/{self.board.pk}/')
        response = self.view(request)
        self.assertEqual(response.status_code, 403)

class BoardArchiveOnTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = BoardArchiveOnAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.board = Board.objects.create(title='Test board', user=self.user)
        self.another_users_board = Board.objects.create(title='Test board')

    def test_board_archive_on_view(self):
        request = self.factory.put(f'api/archive_on/{self.board.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.board.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], self.board.pk)
        self.assertEqual(response.data['archive'], True)

    def test_board_archive_on_view_invalid_id(self):
        invalid_pk = 999
        request = self.factory.put(f'api/archive_on/{invalid_pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=invalid_pk)
        self.assertEqual(response.status_code, 404)

    def test_board_archive_on_view_another_users_board(self):
        self.another_users_board
        request = self.factory.put(f'api/archive_on/{self.another_users_board.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.another_users_board.pk)
        self.assertEqual(response.status_code, 403)

    def test_board_archive_on_view_notauthenticated(self):
        request = self.factory.put(f'api/archive_on/{self.board.pk}/')
        response = self.view(request)
        self.assertEqual(response.status_code, 403)

class BoardArchiveOffTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = BoardArchiveOffAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.board = Board.objects.create(title='Test board', user=self.user, archive=True)
        self.another_users_board = Board.objects.create(title='Test board')

    def test_board_archive_off_view(self):
        request = self.factory.put(f'api/archive_off/{self.board.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.board.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], self.board.pk)
        self.assertEqual(response.data['archive'], False)

    def test_board_archive_off_view_invalid_id(self):
        invalid_pk = 999
        request = self.factory.put(f'api/archive_off/{invalid_pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=invalid_pk)
        self.assertEqual(response.status_code, 404)

    def test_board_archive_off_view_another_users_board(self):
        self.another_users_board
        request = self.factory.put(f'api/archive_off/{self.another_users_board.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.another_users_board.pk)
        self.assertEqual(response.status_code, 403)

    def test_board_archive_off_view_notauthenticated(self):
        request = self.factory.put(f'api/archive_off/{self.board.pk}/')
        response = self.view(request)
        self.assertEqual(response.status_code, 403)

class AddMembersToBoardAPIViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.factory = RequestFactory()
        self.view = AddMembersToBoardAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2', email='testuser@example.com')
        self.board = Board.objects.create(title='Test board', user=self.user)

    def test_add_member_existing_user(self):
        email = self.user2.email
        self.assertEqual(len(mail.outbox), 0)
        url = reverse('member_add_to_board', kwargs={'pk': self.board.pk})
        data = {'email': email}
        request = self.factory.post(url, data)
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.board.pk)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.board.refresh_from_db()
        self.assertEqual(len(self.board.members.all()), 1)
        self.assertEqual(self.board.members.first().email, email)
        self.assertEqual(len(mail.outbox), 0)

    def test_add_member_new_user(self):
        email = 'newuser@example.com'
        self.assertEqual(len(mail.outbox), 0)
        url = reverse('member_add_to_board', kwargs={'pk': self.board.pk})
        data = {'email': email}
        request = self.factory.post(url, data)
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.board.pk)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.board.refresh_from_db()
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Регистрация на сайте')
        self.assertEqual(mail.outbox[0].to, [email])

    def test_add_member_notauthenticated(self):
        email = 'test@gmail.com'
        url = reverse('member_add_to_board', kwargs={'pk': self.board.pk})
        data = {'email': email}
        request = self.factory.post(url, data)
        response = self.view(request, pk=self.board.pk)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(len(mail.outbox), 0)

class RemoveMembersBoardViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.factory = RequestFactory()
        self.view = RemoveMembersBoardView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2', email='testuser@example.com')
        self.board = Board.objects.create(title='Test board')
        self.board.members.set([self.user])

    def test_remove_member_existing_user(self):
        url = reverse('member_remove_board', kwargs={'pk': self.board.pk})
        data = {'members': [self.user.pk, self.user2.pk]}
        request = self.factory.post(url, data)
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.board.pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.board.refresh_from_db()
        self.assertEqual(len(self.board.members.all()), 1)

    def test_remove_member_notauthenticated(self):
        url = reverse('member_remove_board', kwargs={'pk': self.board.pk})
        data = {'members': [self.user.pk, self.user2.pk]}
        request = self.factory.post(url, data)
        response = self.view(request, pk=self.board.pk)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class BoardCreateAPIViewTestCase(APITestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = BoardDetailAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')

    def test_board_create_view(self):
        url = reverse('create_board')
        data = {'title': 'Test Title'}
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['user'], self.user.id)
        self.assertEqual(response.data['title'], 'Test Title')
        self.assertEqual(response.data['members'], [self.user.id])

    def test_board_create_notauthenticated_view(self):
        url = reverse('create_board')
        data = {'title': 'Test Title'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_board_create_bad_request_view(self):
        url = reverse('create_board')
        data = {
            'title': False,
            'background': 23,
            'favorite': 'Hello',
            'archive': 'Hello',
            }
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 400)
