from django.test import (
    TestCase, 
    RequestFactory,
    Client,
)
from django.core import (
    mail,
    )
from rest_framework.test import APITestCase
from .models import (
    Board,
    List,
    Card,
)
from django.contrib.auth import (
    get_user_model,
    )
from rest_framework.test import (
    force_authenticate,
    )
from django.urls import reverse
from django.conf import settings
from .item_views import *


User = get_user_model()

class ItemCreateAPIViewTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.view = ItemCreateAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.board_user = User.objects.create_user(username='boarduser', password='boardpassword')

        self.board = Board.objects.create(
            title='Board 1',
            user=self.board_user,
        )

        self.list = List.objects.create(
            title='List 1',
            owner=self.board_user,
            board=self.board,
        )

    def test_item_create_how_board_user_view(self):
        card = Card.objects.create(title='Test card', owner=self.user, list=self.list)
        checklist = Checklist.objects.create(title='Test checklist title', owner=self.user, card=card)
        request = self.factory.post(f'api/item_create/{checklist.pk}/', {
            "title": "Test Item",
            "assign": self.user.pk,
        }, format='json')

        force_authenticate(request, user=self.board_user)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Item.objects.count(), 1)
        self.assertEqual(response.data['owner'], self.board_user.id)
        self.assertEqual(response.data['checklist'], checklist.id)

    def test_item_create_how_board_member_view(self):
        self.board.members.set([self.user])
        card = Card.objects.create(title='Test card', owner=self.board_user, list=self.list)
        checklist = Checklist.objects.create(title='Test checklist title', owner=self.board_user, card=card)
        request = self.factory.post(f'api/item_create/{checklist.pk}/', {
            "title": "Test Item",
            "assign": self.user.pk,
        }, format='json')

        force_authenticate(request, user=self.user)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Item.objects.count(), 1)
        self.assertEqual(response.data['owner'], self.user.id)
        self.assertEqual(response.data['checklist'], checklist.id)

    def test_item_create_how_card_owner_view(self):
        card = Card.objects.create(title='Test card', owner=self.user, list=self.list)
        checklist = Checklist.objects.create(title='Test checklist title', owner=self.board_user, card=card)
        request = self.factory.post(f'api/item_create/{checklist.pk}/', {
            "title": "Test Item",
            "assign": self.user.pk,
        }, format='json')

        force_authenticate(request, user=self.user)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Item.objects.count(), 1)
        self.assertEqual(response.data['owner'], self.user.id)
        self.assertEqual(response.data['checklist'], checklist.id)

    def test_item_create_how_card_member_view(self):
        card = Card.objects.create(title='Test card', owner=self.board_user, list=self.list)
        card.members.set([self.user])
        checklist = Checklist.objects.create(title='Test checklist title', owner=self.board_user, card=card)
        request = self.factory.post(f'api/item_create/{checklist.pk}/', {
            "title": "Test Item",
            "assign": self.user.pk,
        }, format='json')

        force_authenticate(request, user=self.user)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Item.objects.count(), 1)
        self.assertEqual(response.data['owner'], self.user.id)
        self.assertEqual(response.data['checklist'], checklist.id)

    def test_item_create_how_checklist_user_view(self):
        card = Card.objects.create(title='Test card', owner=self.board_user, list=self.list)
        checklist = Checklist.objects.create(title='Test checklist title', owner=self.user, card=card)
        request = self.factory.post(f'api/item_create/{checklist.pk}/', {
            "title": "Test Item",
            "assign": self.user.pk,
        }, format='json')

        force_authenticate(request, user=self.user)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Item.objects.count(), 1)
        self.assertEqual(response.data['owner'], self.user.id)
        self.assertEqual(response.data['checklist'], checklist.id)

    def test_item_create_how_another_user_view(self):
        card = Card.objects.create(title='Test card', owner = self.user, list=self.list)
        checklist = Checklist.objects.create(title='Test checklist title', owner=self.user, card=card)
        request = self.factory.post(f'api/item_create/{checklist.pk}/', {
            "title": "Test Item",
            "assign": self.user.pk,
        }, format='json')

        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, 403)

    def test_item_create_bad_request_view(self):
        card = Card.objects.create(title='Test card', owner=self.board_user, list=self.list)
        checklist = Checklist.objects.create(title='Test checklist title', owner=self.user, card=card)
        request = self.factory.post(f'api/item_create/{checklist.pk}/', {
            "title": "Test Item",
            "assign": self.user,
        }, format='json')

        force_authenticate(request, user=self.user)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, 400)

class ItemDetailAPIViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ItemDetailAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(title='Test Board', user=self.user)

    def test_checklist_list_for_item_owner_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        item = Item.objects.create(title='Test title', owner=self.user2, checklist=checklist)
        request = self.factory.get(f'api/item_detail/{item.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=item.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], item.pk)

    def test_checklist_list_for_checklist_owner_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user2, card=card)
        item = Item.objects.create(title='Test title', owner=self.user, checklist=checklist)
        request = self.factory.get(f'api/item_detail/{item.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=item.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], item.pk)

    def test_checklist_list_for_card_owner_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user2, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        item = Item.objects.create(title='Test title', owner=self.user, checklist=checklist)
        request = self.factory.get(f'api/item_detail/{item.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=item.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], item.pk)

    def test_checklist_list_for_list_owner_view(self):
        list = List.objects.create(title='Test title', owner=self.user2, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        item = Item.objects.create(title='Test title', owner=self.user, checklist=checklist)
        request = self.factory.get(f'api/item_detail/{item.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=item.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], item.pk)

    def test_checklist_list_for_board_owner_view(self):
        list = List.objects.create(title='Test title', owner=self.user2, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user2, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user2, card=card)
        item = Item.objects.create(title='Test title', owner=self.user2, checklist=checklist)
        request = self.factory.get(f'api/item_detail/{item.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=item.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], item.pk)

    def test_checklist_list_for_card_members_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        card.members.set([self.user2])
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        item = Item.objects.create(title='Test title', owner=self.user, checklist=checklist)
        request = self.factory.get(f'api/item_detail/{item.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=item.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], item.pk)

    def test_checklist_list_for_board_member_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        self.board.members.set([self.user2])
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        item = Item.objects.create(title='Test title', owner=self.user, checklist=checklist)
        request = self.factory.get(f'api/item_detail/{item.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=item.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], item.pk)

    def test_checklist_list_invalid_pk_view(self):
        request = self.factory.get('api/item_detail/99/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=99)
        self.assertEqual(response.status_code, 404)

    def test_checklist_list_not_authenticated_user_view(self):
        request = self.factory.get('api/item_detail/99/')
        response = self.view(request, pk=99)
        self.assertEqual(response.status_code, 403)

class ItemDeleteAPIViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ItemDeleteAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(title='Test Board', user=self.user)

    def test_item_delete_for_board_user_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user2, card=card)
        item = Item.objects.create(title='Test item', owner=self.user2, checklist=checklist)
        request = self.factory.delete(f'api/item_delete/{checklist.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Item.objects.filter(pk=item.pk).exists())

    def test_item_delete_for_list_user_view(self):
        list = List.objects.create(title='Test title', owner=self.user2, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user2, card=card)
        item = Item.objects.create(title='Test item', owner=self.user, checklist=checklist)
        request = self.factory.delete(f'api/item_delete/{checklist.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Item.objects.filter(pk=item.pk).exists())

    def test_item_delete_for_card_user_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user2, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        item = Item.objects.create(title='Test item', owner=self.user, checklist=checklist)
        request = self.factory.delete(f'api/item_delete/{checklist.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Item.objects.filter(pk=item.pk).exists())

    def test_item_delete_for_checklist_user_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user2, card=card)
        item = Item.objects.create(title='Test item', owner=self.user, checklist=checklist)
        request = self.factory.delete(f'api/item_delete/{checklist.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Item.objects.filter(pk=item.pk).exists())

    def test_item_delete_for_item_user_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user2, card=card)
        item = Item.objects.create(title='Test item', owner=self.user2, checklist=checklist)
        request = self.factory.delete(f'api/item_delete/{checklist.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Item.objects.filter(pk=item.pk).exists())

    def test_item_delete_for_list_user_view(self):
        another_user = User.objects.create(username='Test user another', password='password')
        list = List.objects.create(title='Test title', owner=self.user2, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user2, card=card)
        item = Item.objects.create(title='Test item', owner=self.user, checklist=checklist)
        request = self.factory.delete(f'api/item_delete/{checklist.pk}/')
        force_authenticate(request, user=another_user)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, 403)

    def test_item_delete_notauthenticated_user_view(self):
        list = List.objects.create(title='Test title', owner=self.user2, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user2, card=card)
        item = Item.objects.create(title='Test item', owner=self.user, checklist=checklist)
        request = self.factory.delete(f'api/item_delete/{checklist.pk}/')
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, 403)

    def test_item_delete_invalid_pk_view(self):
        request = self.factory.delete('api/item_delete/999/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=999)
        self.assertEqual(response.status_code, 404)

class ItemListAPIViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ItemListAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(title='Test Board', user=self.user)

    def test_item_list_for_checklist_owner_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user2, card=card)
        item = Item.objects.create(title='Test title', owner=self.user, checklist=checklist)
        request = self.factory.get(f'api/item_all/{checklist.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['items']), 1)

    def test_item_list_for_card_owner_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user2, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        item = Item.objects.create(title='Test title', owner=self.user, checklist=checklist)
        request = self.factory.get(f'api/item_all/{checklist.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['items']), 1)

    def test_item_list_for_list_owner_view(self):
        list = List.objects.create(title='Test title', owner=self.user2, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        item = Item.objects.create(title='Test title', owner=self.user, checklist=checklist)
        request = self.factory.get(f'api/item_all/{checklist.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['items']), 1)

    def test_item_list_for_board_owner_view(self):
        list = List.objects.create(title='Test title', owner=self.user2, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user2, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user2, card=card)
        item = Item.objects.create(title='Test title', owner=self.user, checklist=checklist)
        request = self.factory.get(f'api/item_all/{checklist.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['items']), 1)

    def test_item_list_for_card_member_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        card.members.set([self.user2])
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        item = Item.objects.create(title='Test title', owner=self.user, checklist=checklist)
        request = self.factory.get(f'api/item_all/{checklist.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['items']), 1)

    def test_item_list_for_board_member_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        self.board.members.set([self.user2])
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        item = Item.objects.create(title='Test title', owner=self.user, checklist=checklist)
        request = self.factory.get(f'api/item_all/{checklist.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['items']), 1)

    def test_item_list_notauthenticated_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        request = self.factory.get(f'api/item_all/{checklist.pk}/')
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, 404)

class ItemUpdateAPIViewTestCase(APITestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ItemUpdateAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(
            title='Board 1',
            user=self.user,
        )

    def test_item_update_for_board_user_view(self):
        list = List.objects.create(title='Test title', owner=self.user2, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user2, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user2, card=card)
        item = Item.objects.create(title='Test title', owner=self.user2, checklist=checklist)
        url = reverse('item_update', kwargs={'pk': item.pk})
        data = {
            'title': 'Updated item title',
            'assign': self.user2.pk,
        }
        self.client.force_authenticate(user=self.user2)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], checklist.pk)
        self.assertEqual(response.data['title'], 'Updated item title')
        self.assertEqual(response.data['assign'], self.user2.pk)

    def test_item_update_for_board_member_view(self):
        self.board.members.set([self.user2])
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        item = Item.objects.create(title='Test title', owner=self.user, checklist=checklist)
        url = reverse('item_update', kwargs={'pk': item.pk})
        data = {
            'title': 'Updated item title',
            'assign': self.user2.pk,
        }
        self.client.force_authenticate(user=self.user2)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], checklist.pk)
        self.assertEqual(response.data['title'], 'Updated item title')
        self.assertEqual(response.data['assign'], self.user2.pk)

    def test_item_update_for_card_owner_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user2, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        item = Item.objects.create(title='Test title', owner=self.user, checklist=checklist)
        url = reverse('item_update', kwargs={'pk': item.pk})
        data = {
            'title': 'Updated item title',
            'assign': self.user2.pk,
        }
        self.client.force_authenticate(user=self.user2)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], checklist.pk)
        self.assertEqual(response.data['title'], 'Updated item title')
        self.assertEqual(response.data['assign'], self.user2.pk)

    def test_item_update_for_checklist_owner_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user2, card=card)
        item = Item.objects.create(title='Test title', owner=self.user, checklist=checklist)
        url = reverse('item_update', kwargs={'pk': item.pk})
        data = {
            'title': 'Updated item title',
            'assign': self.user2.pk,
        }
        self.client.force_authenticate(user=self.user2)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], checklist.pk)
        self.assertEqual(response.data['title'], 'Updated item title')
        self.assertEqual(response.data['assign'], self.user2.pk)

    def test_item_update_for_item_owner_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        item = Item.objects.create(title='Test title', owner=self.user2, checklist=checklist)
        url = reverse('item_update', kwargs={'pk': item.pk})
        data = {
            'title': 'Updated item title',
            'assign': self.user2.pk,
        }
        self.client.force_authenticate(user=self.user2)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], checklist.pk)
        self.assertEqual(response.data['title'], 'Updated item title')
        self.assertEqual(response.data['assign'], self.user2.pk)

    def test_item_update_notauthenticated_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user2, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        item = Item.objects.create(title='Test title', owner=self.user, checklist=checklist)
        url = reverse('item_update', kwargs={'pk': item.pk})
        data = {
            'title': 'Updated item title',
            'assign': self.user2.pk,
        }
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_item_update_for_another_user_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        item = Item.objects.create(title='Test title', owner=self.user, checklist=checklist)
        url = reverse('item_update', kwargs={'pk': item.pk})
        data = {
            'title': 'Updated item title',
            'assign': self.user2.pk,
        }
        self.client.force_authenticate(user=self.user2)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 403)
