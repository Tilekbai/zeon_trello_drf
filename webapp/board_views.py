from smtplib import SMTPException
from rest_framework.response import (
    Response,
)
from rest_framework import (
    generics,
) 
from rest_framework.views import (
    APIView,
    )
from rest_framework import (
    permissions,
    )
from rest_framework.views import APIView
from rest_framework import status
from django.shortcuts import get_object_or_404
from django.http import Http404
from django.core.mail import send_mail
from django.contrib.auth import get_user_model
from source.settings import DEFAULT_FROM_EMAIL
from .models import (
    Board,
)
from .serializers import (
    BoardSerializer,
)


class BoardListAll(generics.ListAPIView):
    queryset = Board.objects.all()
    serializer_class = BoardSerializer

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset().filter(user=self.request.user.id) 
        serializer = self.get_serializer(queryset, many=True)
        context = {'boards': serializer.data}
        return Response(context)

class BoardCreateView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, format=None):
        data = request.data
        data['user'] = request.user.id
        serializer = BoardSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

class BoardDetailAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, pk, format=None):
        board = get_object_or_404(Board, pk=pk)
        if request.user == board.user or request.user in board.members.all():
            serializer = BoardSerializer(board)
            return Response(serializer.data)
        else:
            raise Http404('Page not found')

class BoardListFavorite(generics.ListAPIView):
    queryset = Board.objects.all().filter(favorite=True)
    serializer_class = BoardSerializer

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset().filter(user=self.request.user.id) 
        serializer = self.get_serializer(queryset, many=True)
        context = {'boards': serializer.data}
        return Response(context)
    
class BoardListArchive(generics.ListAPIView):
    queryset = Board.objects.all().filter(archive=True)
    serializer_class = BoardSerializer

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset().filter(user=self.request.user.id) 
        serializer = self.get_serializer(queryset, many=True)
        context = {'boards': serializer.data}
        return Response(context)

class BoardFavoriteOnAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    def put(self, request, pk, format=None):
        board = get_object_or_404(Board, pk=pk)
        if board.user == request.user:
            board.favorite = True
            board.save()
            serializer = BoardSerializer(board)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

class BoardFavoriteOffAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    def put(self, request, pk, format=None):
        board = get_object_or_404(Board, pk=pk)
        if board.user == request.user:
            board.favorite = False
            board.save()
            serializer = BoardSerializer(board)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

class BoardArchiveOnAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    def put(self, request, pk, format=None):
        board = get_object_or_404(Board, pk=pk)
        if board.user == request.user:
            board.archive = True
            board.save()
            serializer = BoardSerializer(board)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

class BoardArchiveOffAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    def put(self, request, pk, format=None):
        board = get_object_or_404(Board, pk=pk)
        if board.user == request.user:
            board.archive = False
            board.save()
            serializer = BoardSerializer(board)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

class AddMembersToBoardAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    
    def post(self, request, pk, format=None):
        board = get_object_or_404(Board, id=pk)
        email = request.data.get('email')
        user_in_db = get_user_model().objects.filter(email=email).first()

        if user_in_db:
            board.members.add(user_in_db)
        else:
            try:
                send_mail(
                    'Регистрация на сайте',
                    'Здравствуйте! Вам необходимо зарегистрироваться на нашем сайте.',
                    DEFAULT_FROM_EMAIL,
                    [email],
                    fail_silently=False,
                )
            except SMTPException as e:
                print('There was an error sending an email:', e)
                return Response('Invalid header found. MailGun: Free accounts are for test purposes only. Please upgrade or add the address to authorized recipients in Account Settings', status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)

class RemoveMembersBoardView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, pk):
        board = get_object_or_404(Board, id=pk)
        members_ids = request.data.get('members', [])
        choosed_users = get_user_model().objects.filter(pk__in=members_ids)
        
        board.members.remove(*choosed_users)

        for list_obj in board.boards.all():
            for card in list_obj.cards.all():
                card.members.remove(*choosed_users)

        return Response(status=status.HTTP_204_NO_CONTENT)
