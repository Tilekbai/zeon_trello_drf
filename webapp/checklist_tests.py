from django.test import (
    TestCase, 
    RequestFactory,
    Client,
)
from django.core import (
    mail,
    )
from rest_framework.test import APITestCase
from .models import (
    Board,
    List,
    Card,
)
from django.contrib.auth import (
    get_user_model,
    )
from rest_framework.test import (
    force_authenticate,
    )
from django.urls import reverse
from django.conf import settings
from .checklist_views import *


User = get_user_model()


class ChecklistCreateAPIViewTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.view = ChecklistCreateAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.board_user = User.objects.create_user(username='boarduser', password='boardpassword')

        self.board = Board.objects.create(
            title='Board 1',
            user=self.board_user,
        )
        self.board.members.set([self.board_user])

        self.list = List.objects.create(
            title='List 1',
            owner=self.board_user,
            board=self.board,
        )
        self.card = Card.objects.create(
            title='Test title card',
            description='Description',
            owner=self.user,
            list=self.list,
        )
        self.card.members.set([self.user])

    def test_checklist_create_view(self):
        self.board.members.add(self.user.id)
        request = self.factory.post(f'api/checklist_create/{self.card.pk}/', {
            "title": "Test Checklist",
        }, format='json')

        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.card.pk)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Card.objects.count(), 1)
        self.assertEqual(response.data['owner'], self.user.id)
        self.assertEqual(response.data['card'], self.card.id)

    def test_create_checklist_unauthenticated_user(self):
        request = self.factory.post(f'api/checklist_create/{self.card.pk}/', {
            "title": "Test checklist",
        }, format='json')

        response = self.view(request, pk=self.list.pk)
        self.assertEqual(response.status_code, 403)

class ChecklistDetailAPIViewTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.view = ChecklistDetailAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.board_user = User.objects.create_user(username='boarduser', password='boardpassword')
        self.board = Board.objects.create(
            title='Board 1',
            user=self.board_user,
        )
        self.list = List.objects.create(
            title='List 1',
            owner=self.board_user,
            board=self.board,
        )
        self.card = Card.objects.create(
            title='Test title card',
            description='Description',
            owner=self.user,
            list=self.list,
        )
        self.card.members.set([self.user])
        self.checklist = Checklist.objects.create(
            title='Test Checklist title',
            owner = self.user,
            card = self.card,
        )

    def test_checklist_detail_view(self):
        request = self.factory.get(f'api/checklist_detail/{self.checklist.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.checklist.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['owner'], self.user.id)
        self.assertEqual(response.data['card'], self.card.id)

    def test_checklist_detail_notauthenticated_user_view(self):
        request = self.factory.get(f'api/checklist_detail/{self.checklist.pk}/')
        response = self.view(request, pk=self.checklist.pk)
        self.assertEqual(response.status_code, 403)

    def test_checklist_detail_invalid_pk_view(self):
        request = self.factory.get('api/checklist_detail/999/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=999)
        self.assertEqual(response.status_code, 404)

    def test_checklist_detail_norights_user_view(self):
        another_user = User.objects.create(username='anotheruser', password='passwowsdf')
        request = self.factory.get(f'api/checklist_detail/{another_user.pk}/')
        force_authenticate(request, user=another_user)
        response = self.view(request, pk=self.checklist.pk)
        self.assertEqual(response.status_code, 404)

class ChecklistUpdateAPIViewTestCase(APITestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ChecklistUpdateAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(
            title='Board 1',
            user=self.user,
        )
        self.list = List.objects.create(
            title='List 1',
            board=self.board,
        )
        self.card = Card.objects.create(
            title='Card 1',
            list=self.list,
            description='description',
            end_date='2023-07-22',
        )
        self.checklist = Checklist.objects.create(
            title = 'Test title checklist',
            owner = self.user,
            card = self.card,
        )

    def test_card_update_for_board_member_view(self):
        self.list.owner = self.user
        self.card.owner = self.user
        url = reverse('checklist_update', kwargs={'pk': self.checklist.pk})
        data = {
            'title': 'Updated checklist title',
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], self.checklist.pk)
        self.assertEqual(response.data['title'], 'Updated checklist title')

    def test_card_update_notauthenticated_user_view(self):
        self.list.owner = self.user
        self.card.owner = self.user
        url = reverse('checklist_update', kwargs={'pk': self.checklist.pk})
        data = {
            'title': 'Updated checklist title',
        }
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_card_update_badrequest_view(self):
        self.list.owner = self.user
        self.card.owner = self.user
        url = reverse('checklist_update', kwargs={'pk': self.checklist.pk})
        data = {
            'title': False,
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 400)

class ChecklistDeleteViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ChecklistDeleteAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(title='Test Board', user=self.user)

    def test_checklist_delete_for_board_user_view(self):
        list = List.objects.create(title='Test title', owner=self.user2, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user2, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user2, card=card)
        request = self.factory.delete(f'api/checklist_delete/{checklist.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Checklist.objects.filter(pk=checklist.pk).exists())

    def test_checklist_delete_for_list_owner_view(self):
        list = List.objects.create(title='Test title', owner=self.user2, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        request = self.factory.delete(f'api/checklist_delete/{checklist.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Checklist.objects.filter(pk=checklist.pk).exists())

    def test_checklist_delete_for_card_owner_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user2, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        request = self.factory.delete(f'api/checklist_delete/{checklist.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Checklist.objects.filter(pk=checklist.pk).exists())

    def test_checklist_delete_for_checklist_owner_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user2, card=card)
        request = self.factory.delete(f'api/checklist_delete/{checklist.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Checklist.objects.filter(pk=checklist.pk).exists())

    def test_checklist_delete_for_card_member_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        card.members.set([self.user2])
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        request = self.factory.delete(f'api/checklist_delete/{checklist.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Checklist.objects.filter(pk=checklist.pk).exists())

    def test_checklist_delete_notauthenticated_user_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        request = self.factory.delete(f'api/checklist_delete/{checklist.pk}/')
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, 403)

    def test_checklist_delete_another_user_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        request = self.factory.delete(f'api/checklist_delete/{checklist.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=checklist.pk)
        self.assertEqual(response.status_code, 403)

class ChecklistListAPIViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ChecklistListAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(title='Test Board', user=self.user)

    def test_checklist_list_for_card_owner_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user2, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user2, card=card)
        request = self.factory.get(f'api/checklists/{card.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['checklists']), 1)

    def test_checklist_list_for_list_owner_view(self):
        list = List.objects.create(title='Test title', owner=self.user2, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user2, card=card)
        request = self.factory.get(f'api/checklists/{card.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['checklists']), 1)

    def test_checklist_list_for_board_owner_view(self):
        list = List.objects.create(title='Test title', owner=self.user2, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user2, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user2, card=card)
        request = self.factory.get(f'api/checklists/{card.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['checklists']), 1)

    def test_checklist_list_for_card_member_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        card.members.set([self.user2])
        checklist = Checklist.objects.create(title='Test title', owner=self.user2, card=card)
        request = self.factory.get(f'api/checklists/{card.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['checklists']), 1)

    def test_checklist_list_for_notauthenticated_user_view(self):
        list = List.objects.create(title='Test title', owner=self.user2, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user2, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user2, card=card)
        request = self.factory.get(f'api/checklists/{card.pk}/')
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, 404)

    def test_checklist_list_for_another_user_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        request = self.factory.get(f'api/checklists/{card.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, 404)

    def test_checklist_list_for_invalid_pk_view(self):
        list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test title', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test title', owner=self.user, card=card)
        request = self.factory.get(f'api/checklists/999/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, 404)
