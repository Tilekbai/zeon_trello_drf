from django.test import (
    TestCase, 
    RequestFactory,
    Client,
)
from rest_framework.test import APITestCase
from .models import (
    Board,
    List,
    Card,
    Comment,
    Label,
)
from django.contrib.auth import (
    get_user_model,
    )
from rest_framework.test import (
    force_authenticate,
    )
from django.urls import reverse
from django.conf import settings
from .label_views import *


User = get_user_model()

class LabelCreateAPIViewTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.view = LabelCreateAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='boarduser', password='boardpassword')
        self.board = Board.objects.create(
            title='Board 1',
            user=self.user,
        )

    def test_label_create_how_board_user_view(self):
        list = List.objects.create(title='Test list', owner=self.user2, board=self.board)
        card = Card.objects.create(title='Test card', owner=self.user2, list=list)
        request = self.factory.post(f'api/label_create/{card.pk}/', {
            "title": "Test title",
            "color": "green",
            "custom_color": "",
        }, format='json')

        force_authenticate(request, user=self.user)
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Label.objects.count(), 1)
        self.assertEqual(response.data['owner'], self.user.id)
        self.assertEqual(response.data['card'], card.id)

    def test_label_create_how_list_owner_view(self):
        list = List.objects.create(title='Test list', owner=self.user2, board=self.board)
        card = Card.objects.create(title='Test card', owner=self.user, list=list)
        request = self.factory.post(f'api/label_create/{card.pk}/', {
            "title": "Test title",
            "color": "green",
            "custom_color": "",
        }, format='json')

        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Label.objects.count(), 1)
        self.assertEqual(response.data['owner'], self.user2.id)
        self.assertEqual(response.data['card'], card.id)

    def test_label_create_how_card_owner_view(self):
        list = List.objects.create(title='Test list', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test card', owner=self.user2, list=list)
        request = self.factory.post(f'api/label_create/{card.pk}/', {
            "title": "Test title",
            "color": "green",
            "custom_color": "",
        }, format='json')

        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Label.objects.count(), 1)
        self.assertEqual(response.data['owner'], self.user2.id)
        self.assertEqual(response.data['card'], card.id)

    def test_label_create_how_card_owner_view(self):
        list = List.objects.create(title='Test list', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test card', owner=self.user2, list=list)
        request = self.factory.post(f'api/label_create/{card.pk}/', {
            "title": "Test title",
            "color": "green",
            "custom_color": "",
        }, format='json')

        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, 403)

class LabelDetailAPIViewTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.view = LabelDetailAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='boarduser', password='boardpassword')
        self.board = Board.objects.create(
            title='Board 1',
            user=self.user,
        )

    def test_label_detail_how_board_user_view(self):
        list = List.objects.create(title='Test list', owner=self.user2, board=self.board)
        card = Card.objects.create(title='Test card', owner=self.user2, list=list)
        label = Label.objects.create(title='Test label', owner=self.user2, card=card, color='green')
        request = self.factory.get(f'api/label_detail/{label.pk}/')

        force_authenticate(request, user=self.user)
        response = self.view(request, pk=label.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], label.id)

    def test_label_detail_how_list_user_view(self):
        list = List.objects.create(title='Test list', owner=self.user2, board=self.board)
        card = Card.objects.create(title='Test card', owner=self.user, list=list)
        label = Label.objects.create(title='Test label', owner=self.user, card=card, color='green')
        request = self.factory.get(f'api/label_detail/{label.pk}/')

        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=label.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], label.id)

    def test_label_detail_how_card_user_view(self):
        list = List.objects.create(title='Test list', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test card', owner=self.user2, list=list)
        label = Label.objects.create(title='Test label', owner=self.user, card=card, color='green')
        request = self.factory.get(f'api/label_detail/{label.pk}/')

        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=label.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], label.id)

    def test_label_detail_how_label_user_view(self):
        list = List.objects.create(title='Test list', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test card', owner=self.user, list=list)
        label = Label.objects.create(title='Test label', owner=self.user2, card=card, color='green')
        request = self.factory.get(f'api/label_detail/{label.pk}/')

        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=label.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], label.id)

    def test_label_detail_notauthenticated_user_view(self):
        list = List.objects.create(title='Test list', owner=self.user2, board=self.board)
        card = Card.objects.create(title='Test card', owner=self.user, list=list)
        label = Label.objects.create(title='Test label', owner=self.user, card=card, color='green')
        request = self.factory.get(f'api/label_detail/{label.pk}/')

        response = self.view(request, pk=label.pk)
        self.assertEqual(response.status_code, 403)

    def test_label_detail_another_user_view(self):
        list = List.objects.create(title='Test list', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test card', owner=self.user, list=list)
        label = Label.objects.create(title='Test label', owner=self.user, card=card, color='green')
        request = self.factory.get(f'api/label_detail/{label.pk}/')

        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=label.pk)
        self.assertEqual(response.status_code, 404)

class LabelDeleteAPIViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = LabelDeleteAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(title='Test Board', user=self.user)
        self.list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        self.card = Card.objects.create(title='Test title', owner=self.user, list=self.list)
        self.label = Label.objects.create(title='Test title', owner=self.user2, card=self.card, color='green')

    def test_label_delete_label_owner_user_view(self):
        request = self.factory.delete(f'api/label_delete/{self.label.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=self.label.pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Label.objects.filter(pk=self.label.pk).exists())

    def test_label_delete_label_notauthenticated_user_view(self):
        request = self.factory.delete(f'api/label_delete/{self.label.pk}/')
        
        response = self.view(request, pk=self.label.pk)
        self.assertEqual(response.status_code, 403)

    def test_label_delete_label_invalid_pk_view(self):
        request = self.factory.delete('api/label_delete/999/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=999)
        self.assertEqual(response.status_code, 404)

class LabelUpdateAPIViewTestCase(APITestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = LabelUpdateAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(
            title='Board 1',
            user=self.user,
        )
        self.list = List.objects.create(
            title='List 1',
            board=self.board,
            owner = self.user,
        )
        self.card = Card.objects.create(
            title='Card 1',
            list=self.list,
            description='description',
            end_date='2023-07-22',
            owner = self.user,
        )
        self.label = Label.objects.create(
            title = 'Test title label',
            owner = self.user2,
            card = self.card,
            color = 'green',
        )

    def test_label_update_for_label_owner_view(self):
        self.list.owner = self.user
        self.card.owner = self.user
        url = reverse('label_update', kwargs={'pk': self.label.pk})
        data = {
            'title': 'Updated label text',
            'color': 'blue',
        }
        self.client.force_authenticate(user=self.user2)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], self.label.pk)
        self.assertEqual(response.data['title'], 'Updated label text')

    def test_label_update_notauthenticated_user_view(self):
        self.list.owner = self.user
        self.card.owner = self.user
        url = reverse('label_update', kwargs={'pk': self.label.pk})
        data = {
            'title': 'Updated label text',
            'color': 'blue',
        }
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_label_update_for_not_owner_view(self):
        self.list.owner = self.user
        self.card.owner = self.user
        url = reverse('label_update', kwargs={'pk': self.label.pk})
        data = {
            'title': 'Updated label text',
            'color': 'blue',
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_label_update_invalid_pk_view(self):
        self.list.owner = self.user
        self.card.owner = self.user
        url = reverse('label_update', kwargs={'pk': 999})
        data = {
            'title': 'Updated label text',
            'color': 'blue',
        }
        self.client.force_authenticate(user=self.user2)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 404)

    def test_label_update_invalid_data_view(self):
        self.list.owner = self.user
        self.card.owner = self.user
        url = reverse('label_update', kwargs={'pk': self.label.pk})
        data = {
            'title': False,
            'color': True,
        }
        self.client.force_authenticate(user=self.user2)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 400)
