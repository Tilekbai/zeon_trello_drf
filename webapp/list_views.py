from rest_framework.response import (
    Response,
)
from rest_framework import (
    generics,
) 
from rest_framework.views import (
    APIView,
    )
from rest_framework import (
    permissions,
    )
from rest_framework.views import APIView
from rest_framework import status
from django.shortcuts import get_object_or_404
from django.http import Http404
from .models import (
    List,
    Board,
)
from .serializers import (
    ListSerializer,
)


class ListDetailAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, pk, format=None):
        list = get_object_or_404(List, pk=pk)
        if request.user == list.owner or request.user in list.board.members.all():
            serializer = ListSerializer(list)
            return Response(serializer.data)
        else:
            raise Http404('Page not found')

class ListAllAPIView(generics.ListAPIView):
    queryset = List.objects.all()
    serializer_class = ListSerializer

    def get(self, request, *args, **kwargs):
        board = Board.objects.get(pk=kwargs['pk'])
        queryset = self.get_queryset().filter(board=board)
        if board.user == self.request.user or self.request.user in board.members.all():
            serializer = self.get_serializer(queryset, many=True)
            context = {'lists': serializer.data}
            return Response(context)
        else:
            raise Http404("Page not found")

class ListUpdateAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    def put(self, request, pk, format=None):
        list = get_object_or_404(List, pk=pk)
        if list.owner == self.request.user or self.request.user in list.board.members.all():
            serializer = ListSerializer(list, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status=400)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

class ListDeleteAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def delete(self, request, pk, format=None):
        try:
            list = List.objects.get(pk=pk)
            board = list.board
            if board.user == self.request.user or self.request.user in board.members.all() or self.request.user == list.owner:
                list.delete()
        except List.DoesNotExist:
            return Response({'detail': 'List not found.'}, status=404)
        return Response(status=204)

class ListCreateAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, pk, format=None):
        board = Board.objects.get(pk=pk)
        if self.request.user == board.user or self.request.user in board.members.all():
            data = request.data
            data['board'] = board.id
            data['owner'] = request.user.id
            serializer = ListSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=403)
