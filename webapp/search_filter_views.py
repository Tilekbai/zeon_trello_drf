from rest_framework.response import (
    Response,
)
from rest_framework import (
    generics,
) 
from rest_framework.views import (
    APIView,
    )
from rest_framework import (
    permissions,
    )
from rest_framework.views import APIView
from django.contrib.auth import get_user_model
from .models import (
    List,
    Board,
)
from .serializers import (
    ListSerializer,
    BoardSerializer,
    CardSerializer,
)
from django.db.models import Q  
from .models import *


class SearchAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        query = self.request.query_params.get('q')
        
        if query is None:
            return Response({"error": "Please provide a 'q' parameter for the search."}, status=400)
        
        board_list = Board.objects.filter(Q(title__icontains=query) & Q(user=self.request.user))
        list_list = List.objects.filter(Q(title__icontains=query) & Q(owner=self.request.user))
        card_list = Card.objects.filter(Q(title__icontains=query) & Q(owner=self.request.user))
        
        board_serializer = BoardSerializer(board_list, many=True)
        list_serializer = ListSerializer(list_list, many=True)
        card_serializer = CardSerializer(card_list, many=True)
        
        response_data = {
            'board_results': board_serializer.data,
            'list_results': list_serializer.data,
            'card_results': card_serializer.data,
        }
        return Response(response_data)

class FilterAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    def get(self, request, *args, **kwargs):
        user = self.request.query_params.get('user')
        label = self.request.query_params.get('label')
        
        if user:
            username = get_user_model().objects.get(username=user)
            card_list = Card.objects.filter(owner__username=username)
        
        if label:
            card_list = Card.objects.filter(labels__title=label)
        
        serializer = CardSerializer(card_list, many=True)
        return Response(serializer.data)
