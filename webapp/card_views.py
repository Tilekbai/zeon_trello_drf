from rest_framework.response import (
    Response,
)
from rest_framework import (
    generics,
) 
from rest_framework.views import (
    APIView,
    )
from rest_framework import (
    permissions,
    )
from rest_framework.views import APIView
from rest_framework import status
from django.shortcuts import get_object_or_404
from django.http import Http404
from django.contrib.auth import get_user_model
from smtplib import SMTPException
from django.http import Http404
from django.core.mail import send_mail
from source.settings import DEFAULT_FROM_EMAIL
from .models import (
    List,
    Board,
    Card,
)
from .serializers import (
    CardSerializer,
)


class CardListAPIView(generics.ListAPIView):
    queryset = Card.objects.all()
    serializer_class = CardSerializer

    def get(self, request, *args, **kwargs):
        list = List.objects.get(pk=kwargs['pk'])
        queryset = self.get_queryset().filter(list=list)
        if list.owner == self.request.user or list.board.user == self.request.user or self.request.user in list.board.members.all():
            serializer = self.get_serializer(queryset, many=True)
            context = {'cards': serializer.data}
            return Response(context)
        else:
            raise Http404("Page not found")

class CardCreateAPIView(generics.CreateAPIView):
    queryset = Card.objects.all()
    serializer_class = CardSerializer
    permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        list_id = self.kwargs.get('pk')
        list = List.objects.get(pk=list_id)
        if self.request.user == list.board.user or list.owner == self.request.user or self.request.user in list.board.members.all():
            serializer.save(owner=self.request.user, list=list)

class CardDetailAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, pk, format=None):
        card = get_object_or_404(Card, pk=pk)
        if card.owner == self.request.user or self.request.user in card.members.all() or self.request.user == card.list.board.user or card.list.owner == self.request.user or self.request.user in card.list.board.members.all():
            serializer = CardSerializer(card)
            return Response(serializer.data)
        else:
            raise Http404('Page not found')

class CardDeleteAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def delete(self, request, pk, format=None):
        try:
            card = Card.objects.get(pk=pk)
            list = List.objects.get(pk=card.list.id)
            board = card.list.board
            if board.user == self.request.user or self.request.user in board.members.all() or self.request.user == list.owner or card.owner == self.request.user:
                list.delete()
        except Card.DoesNotExist:
            return Response({'detail': 'Card not found.'}, status=404)
        return Response(status=204)

class CardUpdateAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    def put(self, request, pk, format=None):
        card = get_object_or_404(Card, pk=pk)
        if card.owner == self.request.user or self.request.user in card.members.all() or card.list.board.user == self.request.user or card.list.owner == self.request.user or self.request.user in card.list.board.members.all():
            serializer = CardSerializer(card, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status=400)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

class AddMembersToCardAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    
    def post(self, request, pk, format=None):
        card = get_object_or_404(Card, id=pk)
        board = get_object_or_404(Board, id=card.list.board.pk)
        email = request.data.get('email')
        user_in_db = get_user_model().objects.filter(email=email).first()
        if card.owner == self.request.user or card.list.owner == self.request.user or board.user == self.request.user or self.request.user in board.members.all() or self.request.user in card.members.all():
            if user_in_db:
                board.members.add(user_in_db)
                card.members.add(user_in_db)
            else:
                try:
                    send_mail(
                        'Регистрация на сайте',
                        'Здравствуйте! Вам необходимо зарегистрироваться на нашем сайте.',
                        DEFAULT_FROM_EMAIL,
                        [email],
                        fail_silently=False,
                    )
                except SMTPException as e:
                    print('There was an error sending an email:', e)
                    return Response('Invalid header found. MailGun: Free accounts are for test purposes only. Please upgrade or add the address to authorized recipients in Account Settings', status=status.HTTP_400_BAD_REQUEST)
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=403)

class RemoveMembersCardAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, pk):
        card = get_object_or_404(Card, id=pk)
        members_ids = request.data.get('members', [])
        choosed_users = get_user_model().objects.filter(pk__in=members_ids)
        if card.owner == self.request.user:
            card.members.remove(*choosed_users)
        else:
            return Response(status=403)
        return Response(status=status.HTTP_204_NO_CONTENT)
