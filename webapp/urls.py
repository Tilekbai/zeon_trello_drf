from django.urls import (
    path,
)
from django.views.i18n import JavaScriptCatalog
from .board_views import *
from .list_views import *
from .card_views import *
from .checklist_views import *
from .item_views import *
from .comment_views import *
from .label_views import *
from .search_filter_views import *

urlpatterns = [
    path('', BoardListAll.as_view(), name='all_boards'),
    path('create_board/', BoardCreateView.as_view(), name='create_board'),
    path('detail_board/<int:pk>/', BoardDetailAPIView.as_view(), name='board_detail'),
    path('favourite_boards/', BoardListFavorite.as_view(), name='favorite_boards'),
    path('archive_boards/', BoardListArchive.as_view(), name='archive_boards'),
    path('favorite_on/<int:pk>/',BoardFavoriteOnAPIView.as_view(), name='favorite_on'),
    path('favorite_off/<int:pk>/',BoardFavoriteOffAPIView.as_view(), name='favorite_off'),
    path('archive_on/<int:pk>/',BoardArchiveOnAPIView.as_view(), name='archive_on'),
    path('archive_off/<int:pk>/',BoardArchiveOffAPIView.as_view(), name='archive_off'),
    path('member_add_to_board/<int:pk>/', AddMembersToBoardAPIView.as_view(), name='member_add_to_board'),
    path('member_remove_board/<int:pk>/', RemoveMembersBoardView.as_view(), name='member_remove_board'),
    # List
    path('list_detail/<int:pk>/', ListDetailAPIView.as_view(), name='list_detail'),
    path('lists_in_board/<int:pk>/', ListAllAPIView.as_view(), name='list_all'),
    path('list_update/<int:pk>/', ListUpdateAPIView.as_view(), name='update_list'),
    path('list_delete/<int:pk>/', ListDeleteAPIView.as_view(), name='delete_list'),
    path('create_list/<int:pk>/', ListCreateAPIView.as_view(), name='create_list'),
    # Card
    path('lists_cards/<int:pk>/', CardListAPIView.as_view(), name='card_list'),
    path('card_create/<int:pk>/', CardCreateAPIView.as_view(), name='card_create'),
    path('card_detail/<int:pk>/', CardDetailAPIView.as_view(), name='card_detail'),
    path('card_delete/<int:pk>/', CardDeleteAPIView.as_view(), name='card_delete'),
    path('card_update/<int:pk>/', CardUpdateAPIView.as_view(), name='card_update'),
    path('member_add_to_card/<int:pk>/', AddMembersToCardAPIView.as_view(), name='member_add_to_card'),
    path('member_remove_from_card/<int:pk>/', RemoveMembersCardAPIView.as_view(), name='member_remove_from_card'),
    # Checklist
    path('checklist_create/<int:pk>/', ChecklistCreateAPIView.as_view(), name='checklist_create'),
    path('checklist_detail/<int:pk>/', ChecklistDetailAPIView.as_view(), name='checklist_detail'),
    path('checklist_update/<int:pk>/', ChecklistUpdateAPIView.as_view(), name='checklist_update'),
    path('checklist_delete/<int:pk>/', ChecklistDeleteAPIView.as_view(), name='checklist_delete'),
    path('checklists/<int:pk>/', ChecklistListAPIView.as_view(), name='checklist_all'),
    # Item
    path('item_create/<int:pk>/', ItemCreateAPIView.as_view(), name='item_create'),
    path('item_detail/<int:pk>/', ItemDetailAPIView.as_view(), name='item_detail'),
    path('item_delete/<int:pk>/', ItemDeleteAPIView.as_view(), name='item_delete'),
    path('item_all/<int:pk>/', ItemListAPIView.as_view(), name='item_all'),
    path('item_update/<int:pk>/', ItemUpdateAPIView.as_view(), name='item_update'),
    # Comment
    path('comment_create/<int:pk>/', CommentCreateAPIView.as_view(), name='comment_create'),
    path('comment_detail/<int:pk>/', CommentDetailAPIView.as_view(), name='comment_detail'),
    path('comment_delete/<int:pk>/', CommentDeleteAPIView.as_view(), name='comment_delete'),
    path('comment_update/<int:pk>/', CommentUpdateAPIView.as_view(), name='comment_update'),
    # Label
    path('label_create/<int:pk>/', LabelCreateAPIView.as_view(), name='label_create'),
    path('label_detail/<int:pk>/', LabelDetailAPIView.as_view(), name='label_detail'),
    path('label_delete/<int:pk>/', LabelDeleteAPIView.as_view(), name='label_delete'),
    path('label_update/<int:pk>/', LabelUpdateAPIView.as_view(), name='label_update'),
    # Search
    path('search_result/', SearchAPIView.as_view(), name='search'),
    # Filter
    path('filter_result/', FilterAPIView.as_view(), name='filter'),
]