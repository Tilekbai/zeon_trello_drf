from django.test import (
    TestCase, 
    RequestFactory,
)
from rest_framework.test import APITestCase
from .models import (
    Board,
    List,
)
from django.contrib.auth import (
    get_user_model,
    )
from rest_framework.test import (
    force_authenticate,
    )
from django.urls import reverse
from django.conf import settings
from .list_views import *


User = get_user_model()

class ListDetailAPIViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ListDetailAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(
            title='Board 1',
            user=self.user,
        )
        self.board.members.set([self.user])
        self.list = List.objects.create(
            title='List 1',
            owner=self.user,
            board=self.board,
        )
        self.list_2 = List.objects.create(
            title='List 2',
            owner=self.user2,
            board=self.board,
        )

    def test_list_detail_view(self):
        request = self.factory.get(f'/api/list_detail/{self.list.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.list.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], self.list.pk)
        self.assertEqual(response.data['title'], self.list.title)
        self.assertEqual(response.data['owner'], self.user.id)

    def test_list_detail_view_not_found(self):
        invalid_id = 999
        request = self.factory.get(f'/api/list_detail/{invalid_id}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=invalid_id)
        self.assertEqual(response.status_code, 404)

    def test_list_detail_view_notauthenticated(self):
        request = self.factory.get(f'/api/list_detail/{self.list.pk}/')
        response = self.view(request, pk=self.list.pk)
        self.assertEqual(response.status_code, 403)

    def test_list_detail_list2_howboardmember_view(self):
        request = self.factory.get(f'/api/list_detail/{self.list_2.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.list_2.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], self.list_2.pk)
        self.assertEqual(response.data['title'], self.list_2.title)
        self.assertEqual(response.data['owner'], self.user2.id)

    def test_list_detail_list_hownotboardmember_view(self):
        request = self.factory.get(f'/api/list_detail/{self.list.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=self.list.pk)
        self.assertEqual(response.status_code, 404)

class ListAllAPIViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ListAllAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(title='Test board1', user=self.user)
        self.list = List.objects.create(title='Test list1', owner=self.user, board=self.board)
        self.list2 = List.objects.create(title='Test list2', owner=self.user2, board=self.board)
        self.board.members.set([self.user2])

    def test_board_list_all_view(self):
        request = self.factory.get(f'api/lists_in_board/{self.board.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.board.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['lists']), 2)

    def test_board_list_view_empty(self):
        List.objects.all().delete()
        request = self.factory.get(f'api/lists_in_board/{self.board.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.board.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['lists']), 0)

    def test_board_list_all_for_user2_how_memberofboard_view(self):
        request = self.factory.get(f'api/lists_in_board/{self.board.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=self.board.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['lists']), 2)

    def test_board_list_all_notauthenticated_view(self):
        request = self.factory.get(f'api/lists_in_board/{self.board.pk}/')
        response = self.view(request, pk=self.board.pk)
        self.assertEqual(response.status_code, 404)

class ListUpdateAPIViewTestCase(APITestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ListUpdateAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(
            title='Board 1',
            user=self.user,
        )
        self.list = List.objects.create(
            title='List 1',
            owner=self.user,
            board=self.board,
        )
        self.list_2 = List.objects.create(
            title='List 2',
            owner=self.user2,
            board=self.board,
        )

    def test_list_update_how_list_owner_view(self):
        url = reverse('update_list', kwargs={'pk': self.list.pk})
        data = {
            'title': 'Updated title',
            'owner': self.user.id,
            'board': self.board.id,
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], self.list.pk)
        self.assertEqual(response.data['title'], 'Updated title')

    def test_list_update_how_board_member_view(self):
        self.board.members.set([self.user2])
        url = reverse('update_list', kwargs={'pk': self.list.pk})
        data = {
            'title': 'Updated title',
            'owner': self.user.id,
            'board': self.board.id,
        }
        self.client.force_authenticate(user=self.user2)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], self.list.pk)
        self.assertEqual(response.data['title'], 'Updated title')

    def test_list_update_how_another_user_view(self):
        url = reverse('update_list', kwargs={'pk': self.list.pk})
        data = {
            'title': 'Updated title',
            'owner': self.user.id,
            'board': self.board.id,
        }
        self.client.force_authenticate(user=self.user2)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 403)
        
    def test_list_update_notauthenticated_view(self):
        url = reverse('update_list', kwargs={'pk': self.list.pk})
        data = {
            'title': 'Updated title',
            'owner': self.user.id,
            'board': self.board.id,
        }
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 403)

class ListDeleteViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ListDeleteAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(title='Test Board', user=self.user)
        self.board.members.set([self.user2])
        self.list = List.objects.create(
            title='Test title',
            owner=self.user,
            board=self.board
            )

    def test_list_delete_view(self):
        request = self.factory.delete(f'api/list_delete/{self.list.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.list.pk)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(List.objects.filter(pk=self.list.pk).exists())

    def test_list_delete_view_not_found(self):
        request = self.factory.delete(f'api/list_delete/999/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=999)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_list_delete_view_notauthenticated(self):
        request = self.factory.delete(f'api/list_delete/999/')
        response = self.view(request, pk=999)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list_delete_for_member_of_board_view(self):
        request = self.factory.delete(f'api/list_delete/{self.list.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=self.list.pk)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(List.objects.filter(pk=self.list.pk).exists())

    def test_list_delete_for_board_user_not_list_owner_view(self):
        test_list = List.objects.create(title='Test list', owner=self.user2, board=self.board)
        request = self.factory.delete(f'api/list_delete/{test_list.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=test_list.pk)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(List.objects.filter(pk=test_list.pk).exists())

class ListCreateAPIViewTestCase(APITestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ListCreateAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(
            title='Board 1',
            user=self.user,
        )
        self.board.members.set([self.user])

    def test_list_create_how_board_user_view(self):
        url = reverse('create_list', kwargs={'pk': self.board.pk})
        data = {'title': 'Test Title'}
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['owner'], self.user.id)
        self.assertEqual(response.data['board'], self.board.id)

    def test_list_create_notauthenticated_view(self):
        url = reverse('create_list', kwargs={'pk': self.board.pk})
        data = {'title': 'Test Title'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_list_create_how_board_member_view(self):
        self.board.members.set([self.user2])
        url = reverse('create_list', kwargs={'pk': self.board.pk})
        data = {'title': 'Test Title'}
        self.client.force_authenticate(user=self.user2)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['owner'], self.user2.id)
        self.assertEqual(response.data['board'], self.board.id)

    def test_list_create_how_another_user_view(self):
        url = reverse('create_list', kwargs={'pk': self.board.pk})
        data = {'title': 'Test Title'}
        self.client.force_authenticate(user=self.user2)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 403)
