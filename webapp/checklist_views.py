from rest_framework.response import (
    Response,
)
from rest_framework import (
    generics,
) 
from rest_framework.views import (
    APIView,
    )
from rest_framework import (
    permissions,
    )
from rest_framework.views import APIView
from rest_framework import status
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from django.http import Http404
from .models import (
    List,
    Checklist,
    Card,
    Board,
)
from .serializers import (
    ChecklistSerializer,
)

User = get_user_model()   

class ChecklistCreateAPIView(generics.CreateAPIView):
    queryset = Checklist.objects.all()
    serializer_class = ChecklistSerializer
    permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        card_id = self.kwargs.get('pk')
        card = Card.objects.get(pk=card_id)
        list = List.objects.get(pk=card.list.pk)
        board = Board.objects.get(pk=list.board.pk)
        if self.request.user == card.owner or list.owner == self.request.user or board.user == self.request.user or self.request.user in card.members.all() or self.request.user in board.members.all():
            if serializer.is_valid():
                serializer.save(owner=self.request.user, card=card)
                return Response(status=201)            
            return Response(status=400)
        else:
            return Response(status=403)

class ChecklistDetailAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, pk, format=None):
        checklist = get_object_or_404(Checklist, pk=pk)
        card = Card.objects.get(id=checklist.card.pk)
        if checklist.owner == self.request.user or card.owner == self.request.user or self.request.user in card.members.all() or self.request.user == card.list.board.user or card.list.owner == self.request.user or self.request.user == card.list.owner:
            serializer = ChecklistSerializer(checklist)
            return Response(serializer.data)
        else:
            raise Http404('Page not found')

class ChecklistUpdateAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    def put(self, request, pk, format=None):
        checklist = get_object_or_404(Checklist, pk=pk)
        card = Card.objects.get(id=checklist.card.pk)
        if checklist.owner == self.request.user or card.owner == self.request.user or self.request.user in card.members.all() or card.list.board.user == self.request.user or card.list.owner == self.request.user:
            serializer = ChecklistSerializer(checklist, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status=400)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

class ChecklistDeleteAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def delete(self, request, pk, format=None):
        try:
            checklist = Checklist.objects.get(pk=pk)
            card = Card.objects.get(pk=checklist.card.pk)
            list = List.objects.get(pk=card.list.id)
            board = card.list.board
            if self.request.user == checklist.owner or board.user == self.request.user or self.request.user == list.owner or card.owner == self.request.user or self.request.user in card.members.all():
                list.delete()
            else:
                return Response(status=403)
        except Checklist.DoesNotExist:
            return Response({'detail': 'Card not found.'}, status=404)
        return Response(status=204)

class ChecklistListAPIView(generics.ListAPIView):
    queryset = Checklist.objects.all()
    serializer_class = ChecklistSerializer

    def get(self, request, *args, **kwargs):
        card = Card.objects.get(pk=kwargs['pk'])
        list = List.objects.get(pk=card.list.pk)
        board = Board.objects.get(pk=list.board.pk)
        queryset = self.get_queryset().filter(card=card)
        if card.owner == self.request.user or self.request.user in card.members.all() or list.owner == self.request.user or board.user == self.request.user or self.request.user in board.members.all():
            serializer = self.get_serializer(queryset, many=True)
            context = {'checklists': serializer.data}
            return Response(context)
        else:
            raise Http404("Page not found")
