from django.test import (
    TestCase, 
    RequestFactory,
    Client,
)
from django.core import (
    mail,
    )
from rest_framework.test import APITestCase
from .models import (
    Board,
    List,
    Card,
)
from django.contrib.auth import (
    get_user_model,
    )
from rest_framework.test import (
    force_authenticate,
    )
from django.urls import reverse
from django.conf import settings
from .search_filter_views import *


User = get_user_model()

class SearchAPIViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = SearchAPIView.as_view()
        self.user2 = User.objects.create_user(username='testuser', password='testpassword')
        self.user = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(title='Test board1', user=self.user)

    def test_search_for_board_owner_user_view(self):
        request = self.factory.get(f'api/search_result/?q=Test%20board1')
        force_authenticate(request, user=self.user)
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['board_results']), 1)

    def test_search_for_list_owner_user_view(self):
        list = List.objects.create(title='list', owner=self.user2, board=self.board)
        request = self.factory.get(f'api/search_result/?q=list')
        force_authenticate(request, user=self.user2)
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['list_results']), 1)

    def test_search_for_card_owner_user_view(self):
        list = List.objects.create(title='Test list1', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test card1', owner=self.user2, list_id=list.id)
        request = self.factory.get(f'api/search_result/?q=Test%20card1')
        force_authenticate(request, user=self.user2)
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['card_results']), 1)

    def test_search_for_board_owner_emptyresult_user_view(self):
        request = self.factory.get(f'api/search_result/?q=anothertitle')
        force_authenticate(request, user=self.user)
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['board_results']), 0)

    def test_search_for_list_owner_empty_result_view(self):
        list = List.objects.create(title='list', owner=self.user2, board=self.board)
        request = self.factory.get(f'api/search_result/?q=anotherlist')
        force_authenticate(request, user=self.user2)
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['list_results']), 0)

    def test_search_for_card_owner_empty_result_view(self):
        list = List.objects.create(title='Test list1', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test card1', owner=self.user2, list_id=list.id)
        request = self.factory.get(f'api/search_result/?q=anothercard1')
        force_authenticate(request, user=self.user2)
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['card_results']), 0)

    def test_search_notquery_user_view(self):
        request = self.factory.get(f'api/search_result/')
        force_authenticate(request, user=self.user2)
        response = self.view(request)
        self.assertEqual(response.status_code, 400)

class FilterAPIViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = FilterAPIView.as_view()
        self.user2 = User.objects.create_user(username='testuser', password='testpassword')
        self.user = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(title='Test board1', user=self.user)
        self.list = List.objects.create(title='Test list', owner=self.user, board=self.board)
        self.card = Card.objects.create(title='Test card', owner=self.user, list=self.list)
        self.label = Label.objects.create(title='Done', owner=self.user, card=self.card)

    def test_filter_by_label_view(self):
        request = self.factory.get(f'api/filter_result/?label={self.label.title}')
        force_authenticate(request, user=self.user)
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0]['id'], self.card.id)

    def test_filter_by_username_view(self):
        request = self.factory.get(f'api/filter_result/?user={self.user.username}')
        force_authenticate(request, user=self.user)
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0]['id'], self.card.id)

    def test_filter_notauthenticated_user_view(self):
        request = self.factory.get(f'api/filter_result/?user={self.user.username}')
        force_authenticate(request)
        response = self.view(request)
        self.assertEqual(response.status_code, 403)
