from rest_framework import (
    serializers,
)
from django.contrib.auth.models import (
    User,
)
from .models import (
    Board,
    List,
    Card,
    Checklist,
    Item,
    Comment,
    Label,
)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name']

class Base64ImageField(serializers.ImageField):
    def to_internal_value(self, data):
        from django.core.files.base import ContentFile
        import base64
        import six
        import uuid

        if isinstance(data, six.string_types):
            if 'data:' in data and ';base64,' in data:
                header, data = data.split(';base64,')
            try:
                decoded_file = base64.b64decode(data+"==")
            except TypeError:
                self.fail('invalid_image')

            file_name = str(uuid.uuid4())[:12]
            file_extension = self.get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension, )

            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr

        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension
    
class BoardSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), allow_null=True)
    title = serializers.CharField(max_length=100)
    background = serializers.ImageField(allow_null=True, required=False)
    favorite = serializers.BooleanField(default=False)
    archive = serializers.BooleanField(default=False)
    members = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), allow_null=True, many=True, required=False)

    def create(self, validated_data):
        board = Board.objects.create(**validated_data)
        board.members.set([validated_data['user']])
        return board

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.background = validated_data.get('background', instance.background)
        instance.favourite = validated_data.get('favourite', instance.favourite)
        instance.archive = validated_data.get('archive', instance.archive)
        instance.members = validated_data.get('members', instance.members)
        instance.save()
        return instance

class ListSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    owner = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), allow_null=True)
    title = serializers.CharField(max_length=30)
    board = serializers.PrimaryKeyRelatedField(queryset=Board.objects.all(), allow_null=True)

    def create(self, validated_data):
        list_instance = List.objects.create(**validated_data)
        return list_instance
    
    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.save()
        return instance

class CardSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    owner = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), allow_null=True, required=False)
    title = serializers.CharField(max_length=30, required=False)
    list = serializers.PrimaryKeyRelatedField(queryset=List.objects.all(), allow_null=True, required=False)
    description = serializers.CharField(style={'base_template': 'textarea.html'}, required=False)
    members = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), allow_null=True, many=True, required=False)
    end_date = serializers.DateField(required=False)
    attachment = serializers.FileField(required=False)

    def create(self, validated_data):
        members_data = validated_data.pop('members', None)
        card = Card.objects.create(**validated_data)
        
        if members_data:
            card.members.set(members_data)
        return card
    
    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get('description', instance.description)
        instance.end_date = validated_data.get('end_date', instance.end_date)
        instance.attachment = validated_data.get('attachment', instance.attachment)
        members_data = validated_data.pop('members', None)
        if members_data:
            instance.members.set(members_data)
        instance.save()
        return instance

class ChecklistSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    owner = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), allow_null=True, required=False)
    title = serializers.CharField(max_length=50, required=False)
    card = serializers.PrimaryKeyRelatedField(queryset=Card.objects.all(), allow_null=True, required=False)

    def create(self, validated_data):
        checklist = Checklist.objects.create(**validated_data)
        return checklist
    
    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.save()
        return instance

class ItemSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    owner = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), allow_null=True, required=False)
    title = serializers.CharField(max_length=50, required=False)
    checklist = serializers.PrimaryKeyRelatedField(queryset=Checklist.objects.all(), allow_null=True, required=False)
    assign = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), allow_null=True, required=False)

    def create(self, validated_data):
        label = Item.objects.create(**validated_data)
        return label
    
    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.assign = validated_data.get('assign', instance.assign)
        instance.save()
        return instance

class CommentSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    author = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), allow_null=True, required=False)
    text = serializers.CharField(max_length=300, required=True)
    card = serializers.PrimaryKeyRelatedField(queryset=Card.objects.all(), allow_null=True, required=False)
    created_at = serializers.DateField(read_only=True, format="%Y-%m-%d")

    def create(self, validated_data):
        label = Comment.objects.create(**validated_data)
        return label
    
    def update(self, instance, validated_data):
        instance.text = validated_data.get('text', instance.text)
        instance.save()
        return instance

class LabelSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    owner = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), allow_null=True, required=False)
    title = serializers.CharField(max_length=30, required=False)
    card = serializers.PrimaryKeyRelatedField(queryset=Card.objects.all(), allow_null=True, required=False)
    color = serializers.CharField(max_length=20)
    custom_color = serializers.CharField(max_length=20, required=False)

    def create(self, validated_data):
        label = Label.objects.create(**validated_data)
        return label
    
    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.color = validated_data.get('color', instance.color)
        instance.custom_color = validated_data.get('custom_color', instance.custom_color)
        instance.save()
        return instance
