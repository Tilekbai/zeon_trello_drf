from rest_framework.response import (
    Response,
)
from rest_framework import (
    generics,
) 
from rest_framework.views import (
    APIView,
    )
from rest_framework import (
    permissions,
    )
from rest_framework.views import APIView
from rest_framework import status
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from django.http import Http404
from .models import (
    List,
    Checklist,
    Card,
    Board,
    Item,
    Comment,
)
from .serializers import (
    CommentSerializer,
)

User = get_user_model()   

class CommentCreateAPIView(generics.CreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        card = Card.objects.get(pk=self.kwargs['pk'])
        checklists = Checklist.objects.filter(card=card)
        list = List.objects.get(pk=card.list.pk)
        board = Board.objects.get(pk=list.board.pk)
        if self.request.user in checklists or self.request.user == card.owner or list.owner == self.request.user or board.user == self.request.user or self.request.user in card.members.all() or self.request.user in board.members.all():
            if serializer.is_valid():
                serializer.save(author=self.request.user, card=card)
                return Response(status=201)
            return Response(status=400)
        else:
            return Response(status=403)

class CommentDetailAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, pk, format=None):
        comment = get_object_or_404(Comment, pk=pk)
        card = Card.objects.get(id=comment.card.pk)
        list = List.objects.get(pk=card.list.id)
        board = Board.objects.get(pk=list.board.id)
        if comment.author == self.request.user or card.owner == self.request.user or self.request.user in card.members.all() or self.request.user == board.user or list.owner == self.request.user:
            serializer = CommentSerializer(comment)
            return Response(serializer.data)
        else:
            raise Http404('Page not found')

class CommentDeleteAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def delete(self, request, pk, format=None):
        try:
            comment = Comment.objects.get(pk=pk)
            if comment.author == self.request.user:
                comment.delete()
            else:
                return Response(status=403)
        except Comment.DoesNotExist:
            return Response({'detail': 'Card not found.'}, status=404)
        return Response(status=204)

class CommentUpdateAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    def put(self, request, pk, format=None):
        comment = get_object_or_404(Comment, pk=pk)
        card = Card.objects.get(pk=comment.card.pk)
        if comment.author == self.request.user:
            serializer = CommentSerializer(comment, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status=400)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)
