from django.test import (
    TestCase, 
    RequestFactory,
    Client,
)
from django.core import (
    mail,
    )
from rest_framework.test import APITestCase
from .models import (
    Board,
    List,
    Card,
)
from django.contrib.auth import (
    get_user_model,
    )
from rest_framework.test import (
    force_authenticate,
    )
from django.urls import reverse
from django.conf import settings
from .card_views import *


User = get_user_model()

class CardListAPIViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = CardListAPIView.as_view()
        self.board_owner_user = User.objects.create_user(username='testuser', password='testpassword')
        self.user = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(title='Test board1', user=self.board_owner_user)

    def test_card_list_for_board_owner_user_view(self):
        list = List.objects.create(title='Test list1', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test card1', owner=self.user, list_id=list.id)
        request = self.factory.get(f'api/lists_cards/{list.pk}/')
        force_authenticate(request, user=self.board_owner_user)
        response = self.view(request, pk=list.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['cards']), 1)

    def test_card_list_for_board_member_user_view(self):
        list = List.objects.create(title='Test list1', owner=self.board_owner_user, board=self.board)
        card = Card.objects.create(title='Test card1', owner=self.board_owner_user, list_id=list.id)
        self.board.members.set([self.user])
        request = self.factory.get(f'api/lists_cards/{list.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=list.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['cards']), 1)

    def test_card_list_for_list_owner_user_view(self):
        list = List.objects.create(title='Test list1', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test card1', owner=self.board_owner_user, list_id=list.id)
        request = self.factory.get(f'api/lists_cards/{list.pk}/')
        force_authenticate(request, user=self.board_owner_user)
        response = self.view(request, pk=list.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['cards']), 1)

    def test_card_list_view_empty(self):
        list = List.objects.create(title='Test list1', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test card1', owner=self.board_owner_user, list_id=list.id)
        Card.objects.all().delete()
        request = self.factory.get(f'api/lists_cards/{list.pk}/')
        force_authenticate(request, user=self.board_owner_user)
        response = self.view(request, pk=self.board.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['cards']), 0)

    def test_card_list_for_list_owner_user_view(self):
        list = List.objects.create(title='Test list1', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test card1', owner=self.board_owner_user, list_id=list.id)
        request = self.factory.get(f'api/lists_cards/{list.pk}/')
        response = self.view(request, pk=list.pk)
        self.assertEqual(response.status_code, 404)

class CardCreateAPIViewTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.view = CardCreateAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.board_user = User.objects.create_user(username='boarduser', password='boardpassword')

        self.board = Board.objects.create(
            title='Board 1',
            user=self.board_user,
        )
        self.board.members.set([self.board_user])

        self.list = List.objects.create(
            title='List 1',
            owner=self.board_user,
            board=self.board,
        )

    def test_card_create_how_board_member_view(self):
        self.board.members.add(self.user.id)
        request = self.factory.post(f'api/card_create/{self.list.pk}/', {
            "title": "Test Card",
            "description": "This is a test card.",
            "members": [self.user.id],
            "end_date": "2023-08-15",
        }, format='json')

        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.list.pk)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Card.objects.count(), 1)
        self.assertEqual(response.data['owner'], self.user.id)
        self.assertEqual(response.data['list'], self.list.id)

    def test_card_create_how_board_owner_view(self):
        request = self.factory.post(f'api/card_create/{self.list.pk}/', {
            "title": "Test Card",
            "description": "This is a test card.",
            "members": [self.user.id],
            "end_date": "2023-08-15",
        }, format='json')

        force_authenticate(request, user=self.board_user)
        response = self.view(request, pk=self.list.pk)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Card.objects.count(), 1)
        self.assertEqual(response.data['owner'], self.board_user.id)
        self.assertEqual(response.data['list'], self.list.id)

    def test_create_card_unauthenticated_user(self):
        request = self.factory.post(f'api/card_create/{self.list.pk}/', {
            "title": "Test Card",
            "description": "This is a test card.",
            "members": [self.user.id],
            "end_date": "2023-08-15",
        }, format='json')

        response = self.view(request, pk=self.list.pk)
        self.assertEqual(response.status_code, 403)

    def test_create_card_invalid_data(self):
        request = self.factory.post(f'api/card_create/{self.list.pk}/', {
            "title": "Test Card",
            "description": "This is a test card.",
            "members": "Bad",
            "end_date": "23223232",
        }, format='json')

        force_authenticate(request, user=self.board_user)
        response = self.view(request, pk=self.list.pk)
        self.assertEqual(response.status_code, 400)

class CardDetailAPIViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = CardDetailAPIView.as_view()
        self.board_owner_user = User.objects.create_user(username='testuser', password='testpassword')
        self.user = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(title='Test board1', user=self.board_owner_user)

    def test_card_detail_for_board_owner_user_view(self):
        list = List.objects.create(title='Test list1', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test card1', owner=self.user, list_id=list.id)
        request = self.factory.get(f'api/card_detail/{card.pk}/')
        force_authenticate(request, user=self.board_owner_user)
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], card.id)

    def test_card_detail_for_board_member_user_view(self):
        self.board.members.add(self.user.id)
        list = List.objects.create(title='Test list1', owner=self.board_owner_user, board=self.board)
        card = Card.objects.create(title='Test card1', owner=self.board_owner_user, list_id=list.id)
        request = self.factory.get(f'api/card_detail/{card.pk}/')
        force_authenticate(request, user=self.board_owner_user)
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], card.id)

    def test_card_detail_for_card_user_view(self):
        list = List.objects.create(title='Test list1', owner=self.board_owner_user, board=self.board)
        card = Card.objects.create(title='Test card1', owner=self.user, list_id=list.id)
        request = self.factory.get(f'api/card_detail/{card.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], card.id)

    def test_card_detail_for_list_owner_view(self):
        list = List.objects.create(title='Test list1', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test card1', owner=self.board_owner_user, list_id=list.id)
        request = self.factory.get(f'api/card_detail/{card.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], card.id)

    def test_card_detail_for_card_member_user_view(self):
        list = List.objects.create(title='Test list1', owner=self.board_owner_user, board=self.board)
        card = Card.objects.create(title='Test card1', owner=self.board_owner_user, list_id=list.id)
        card.members.add(self.user.id)
        request = self.factory.get(f'api/card_detail/{card.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], card.id)

    def test_card_detail_notauthenticated_user(self):
        list = List.objects.create(title='Test list1', owner=self.board_owner_user, board=self.board)
        card = Card.objects.create(title='Test card1', owner=self.board_owner_user, list_id=list.id)
        request = self.factory.get(f'api/card_detail/{card.pk}/')
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, 403)

class CardDeleteViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = CardDeleteAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(title='Test Board', user=self.user)
        self.board.members.set([self.user2])

    def test_list_delete_for_board_user_view(self):
        list = List.objects.create(
            title='Test title',
            owner=self.user2,
            board=self.board
            )
        self.card = Card.objects.create(title='Test Card', description='Test Description', owner=self.user2, list=list)
        request = self.factory.delete(f'api/card_delete/{self.card.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.card.pk)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Card.objects.filter(pk=self.card.pk).exists())

    def test_list_delete_for_list_owner_view(self):
        list = List.objects.create(
            title='Test title',
            owner=self.user2,
            board=self.board
            )
        self.card = Card.objects.create(title='Test Card', description='Test Description', owner=self.user, list=list)
        request = self.factory.delete(f'api/card_delete/{self.card.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=self.card.pk)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Card.objects.filter(pk=self.card.pk).exists())

    def test_list_delete_for_card_owner_view(self):
        list = List.objects.create(
            title='Test title',
            owner=self.user,
            board=self.board
            )
        self.card = Card.objects.create(title='Test Card', description='Test Description', owner=self.user2, list=list)
        request = self.factory.delete(f'api/card_delete/{self.card.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=self.card.pk)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Card.objects.filter(pk=self.card.pk).exists())

    def test_list_delete_notauthenticated_user_view(self):
        list = List.objects.create(
            title='Test title',
            owner=self.user,
            board=self.board
            )
        self.card = Card.objects.create(title='Test Card', description='Test Description', owner=self.user2, list=list)
        request = self.factory.delete(f'api/card_delete/{self.card.pk}/')
        response = self.view(request, pk=self.card.pk)

        self.assertEqual(response.status_code, 403)

class CardUpdateAPIViewTestCase(APITestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = CardUpdateAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(
            title='Board 1',
            user=self.user,
        )
        self.list = List.objects.create(
            title='List 1',
            board=self.board,
        )
        self.card = Card.objects.create(
            title='Card 1',
            list=self.list,
            description='description',
            end_date='2023-07-22',
        )

    def test_card_update_for_board_member_view(self):
        self.list.owner = self.user
        self.card.owner = self.user
        url = reverse('card_update', kwargs={'pk': self.card.pk})
        data = {
            'title': 'Updated title',
            'description': 'Test description updated',
            'end_date': '2023-07-22',
        }
        self.board.members.add(self.user2)
        self.client.force_authenticate(user=self.user)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], self.card.pk)
        self.assertEqual(response.data['title'], 'Updated title')

    def test_card_update_for_board_owner_view(self):
        self.list.owner = self.user2
        self.card.owner = self.user2
        url = reverse('card_update', kwargs={'pk': self.card.pk})
        data = {
            'title': 'Updated title',
            'description': 'Test description updated',
            'end_date': '2023-07-22',
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], self.card.pk)
        self.assertEqual(response.data['title'], 'Updated title')

    def test_card_update_for_list_owner_view(self):
        self.list.owner = self.user2
        self.card.owner = self.user
        url = reverse('card_update', kwargs={'pk': self.card.pk})
        data = {
            'title': 'Updated title',
            'description': 'Test description updated',
            'end_date': '2023-07-22',
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], self.card.pk)
        self.assertEqual(response.data['title'], 'Updated title')

    def test_card_update_for_card_owner_view(self):
        card = Card.objects.create(
            owner = self.user2,
            title='Card 1',
            list=self.list,
            description='description',
            end_date='2023-07-22',
        )
        url = reverse('card_update', kwargs={'pk': card.pk})
        data = {
            'title': 'Updated title',
            'description': 'Test description updated',
            'end_date': '2023-07-22',
        }
        self.client.force_authenticate(user=self.user2)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], card.pk)
        self.assertEqual(response.data['title'], 'Updated title')

    def test_card_update_for_card_member_view(self):
        self.list.owner = self.user
        self.card.owner = self.user
        url = reverse('card_update', kwargs={'pk': self.card.pk})
        data = {
            'title': 'Updated title',
            'description': 'Test description updated',
            'end_date': '2023-07-22',
        }
        self.card.members.add(self.user2)
        self.client.force_authenticate(user=self.user2)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], self.card.pk)
        self.assertEqual(response.data['title'], 'Updated title')

    def test_card_update_notauthenticated_user_view(self):
        self.list.owner = self.user
        self.card.owner = self.user
        url = reverse('card_update', kwargs={'pk': self.card.pk})
        data = {
            'title': 'Updated title',
            'description': 'Test description updated',
            'end_date': '2023-07-22',
        }
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_card_update_bad_request_view(self):
        self.list.owner = self.user
        self.card.owner = self.user
        url = reverse('card_update', kwargs={'pk': self.card.pk})
        data = {
            'title': 'Updated title',
            'description': True,
            'end_date': '2023asdf3',
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 400)

class AddMembersToCardAPIViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.factory = RequestFactory()
        self.view = AddMembersToCardAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword', email='testuser1@email.com')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2', email='testuser@example.com')
        self.board = Board.objects.create(
            title='Board 1',
            user=self.user,
        )
        self.list = List.objects.create(
            title='List 1',
            board=self.board,
        )
        self.card = Card.objects.create(
            title='Card 1',
            owner=self.user2,
            list=self.list,
            description='description',
            end_date='2023-07-22',
        )


    def test_board_user_adds_member_tocard_existing_user(self):
        email = self.user2.email
        self.assertEqual(len(mail.outbox), 0)
        url = reverse('member_add_to_card', kwargs={'pk': self.card.pk})
        data = {'email': email}
        request = self.factory.post(url, data)
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.card.pk)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.board.refresh_from_db()
        self.assertEqual(len(self.board.members.all()), 1)
        self.assertEqual(self.board.members.first().email, email)
        self.assertEqual(len(mail.outbox), 0)

    def test_card_owner_adds_member_tocard_existing_user(self):
        email = self.user.email
        self.assertEqual(len(mail.outbox), 0)
        url = reverse('member_add_to_card', kwargs={'pk': self.card.pk})
        data = {'email': email}
        request = self.factory.post(url, data)
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=self.card.pk)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.board.refresh_from_db()
        self.assertEqual(len(self.board.members.all()), 1)
        self.assertEqual(self.board.members.first().email, email)
        self.assertEqual(len(mail.outbox), 0)

    def test_add_member_new_user(self):
        email = 'newuser@example.com'
        self.assertEqual(len(mail.outbox), 0)
        url = reverse('member_add_to_board', kwargs={'pk': self.board.pk})
        data = {'email': email}
        request = self.factory.post(url, data)
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.card.pk)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.board.refresh_from_db()
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Регистрация на сайте')
        self.assertEqual(mail.outbox[0].to, [email])

    def test_add_member_notauthenticated(self):
        email = 'test@gmail.com'
        url = reverse('member_add_to_board', kwargs={'pk': self.card.pk})
        data = {'email': email}
        request = self.factory.post(url, data)
        response = self.view(request, pk=self.card.pk)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(len(mail.outbox), 0)

class RemoveMembersCardViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.factory = RequestFactory()
        self.view = RemoveMembersCardAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2', email='testuser@example.com')
        self.board = Board.objects.create(title='Test board', user=self.user)
        self.list = List.objects.create(title='Test list', owner=self.user, board=self.board)
        self.card = Card.objects.create(title='Test title card', list=self.list, owner=self.user)
        self.card.members.set([self.user, self.user2])

    def test_remove_member(self):
        url = reverse('member_remove_from_card', kwargs={'pk': self.card.pk})
        data = {'members': [self.user2.pk]}
        request = self.factory.post(url, data)
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.card.pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.board.refresh_from_db()
        self.assertEqual(len(self.card.members.all()), 1)

    def test_remove_member_notauthenticated(self):
        url = reverse('member_remove_from_card', kwargs={'pk': self.card.pk})
        data = {'members': [self.user.pk, self.user2.pk]}
        request = self.factory.post(url, data)
        response = self.view(request, pk=self.card.pk)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_remove_member_not_card_owner(self):
        url = reverse('member_remove_from_card', kwargs={'pk': self.card.pk})
        data = {'members': [self.user.pk, self.user.pk]}
        request = self.factory.post(url, data)
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=self.card.pk)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
