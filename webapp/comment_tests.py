from django.test import (
    TestCase, 
    RequestFactory,
    Client,
)
from django.core import (
    mail,
    )
from rest_framework.test import APITestCase
from .models import (
    Board,
    List,
    Card,
    Comment,
)
from django.contrib.auth import (
    get_user_model,
    )
from rest_framework.test import (
    force_authenticate,
    )
from django.urls import reverse
from django.conf import settings
from .comment_views import *


User = get_user_model()

class CommentCreateAPIViewTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.view = CommentCreateAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.board_user = User.objects.create_user(username='boarduser', password='boardpassword')
        self.board = Board.objects.create(
            title='Board 1',
            user=self.board_user,
        )

    def test_comment_create_how_board_user_view(self):
        list = List.objects.create(title='Test list', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test card', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test checklist title', owner=self.user, card=card)
        request = self.factory.post(f'api/comment_create/{card.pk}/', {
            "text": "Test Text",
        }, format='json')

        force_authenticate(request, user=self.board_user)
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Comment.objects.count(), 1)
        self.assertEqual(response.data['author'], self.board_user.id)
        self.assertEqual(response.data['card'], card.id)

    def test_comment_create_how_board_member_view(self):
        self.board.members.set([self.user])
        list = List.objects.create(title='Test list', owner=self.board_user, board=self.board)
        card = Card.objects.create(title='Test card', owner=self.board_user, list=list)
        checklist = Checklist.objects.create(title='Test checklist title', owner=self.board_user, card=card)
        request = self.factory.post(f'api/comment_create/{card.pk}/', {
            "text": "Test Text",
        }, format='json')

        force_authenticate(request, user=self.user)
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Comment.objects.count(), 1)
        self.assertEqual(response.data['author'], self.user.id)
        self.assertEqual(response.data['card'], card.id)

    def test_comment_create_how_card_member_view(self):
        list = List.objects.create(title='Test list', owner=self.board_user, board=self.board)
        card = Card.objects.create(title='Test card', owner=self.board_user, list=list)
        card.members.set([self.user])
        checklist = Checklist.objects.create(title='Test checklist title', owner=self.board_user, card=card)
        request = self.factory.post(f'api/comment_create/{card.pk}/', {
            "text": "Test Text",
        }, format='json')

        force_authenticate(request, user=self.user)
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Comment.objects.count(), 1)
        self.assertEqual(response.data['author'], self.user.id)
        self.assertEqual(response.data['card'], card.id)

    def test_comment_create_how_card_user_view(self):
        list = List.objects.create(title='Test list', owner=self.board_user, board=self.board)
        card = Card.objects.create(title='Test card', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test checklist title', owner=self.board_user, card=card)
        request = self.factory.post(f'api/comment_create/{card.pk}/', {
            "text": "Test Text",
        }, format='json')

        force_authenticate(request, user=self.user)
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Comment.objects.count(), 1)
        self.assertEqual(response.data['author'], self.user.id)
        self.assertEqual(response.data['card'], card.id)

    def test_comment_create_how_list_user_view(self):
        list = List.objects.create(title='Test list', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test card', owner=self.user, list=list)
        checklist = Checklist.objects.create(title='Test checklist title', owner=self.board_user, card=card)
        request = self.factory.post(f'api/comment_create/{card.pk}/', {
            "text": "Test Text",
        }, format='json')

        force_authenticate(request, user=self.user)
        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Comment.objects.count(), 1)
        self.assertEqual(response.data['author'], self.user.id)
        self.assertEqual(response.data['card'], card.id)

    def test_comment_create_how_notauthenticated_user_view(self):
        list = List.objects.create(title='Test list', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test card', owner = self.user, list=list)
        checklist = Checklist.objects.create(title='Test checklist title', owner=self.user, card=card)
        request = self.factory.post(f'api/comment_create/{card.pk}/', {
            "title": "Test Item",
            "assign": self.user.pk,
        }, format='json')

        response = self.view(request, pk=card.pk)
        self.assertEqual(response.status_code, 403)

class CommentDetailAPIViewTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.view = CommentDetailAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='boarduser', password='boardpassword')
        self.board = Board.objects.create(
            title='Board 1',
            user=self.user,
        )

    def test_comment_detail_for_board_user_view(self):
        list = List.objects.create(title='Test list', owner=self.user2, board=self.board)
        card = Card.objects.create(title='Test card', owner=self.user2, list=list)
        comment = Comment.objects.create(text='Test comment', author=self.user2, card=card)
        request = self.factory.get(f'api/comment_detail/{comment.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=comment.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], comment.id)

    def test_comment_detail_for_list_owner_view(self):
        list = List.objects.create(title='Test list', owner=self.user2, board=self.board)
        card = Card.objects.create(title='Test card', owner=self.user, list=list)
        comment = Comment.objects.create(text='Test comment', author=self.user, card=card)
        request = self.factory.get(f'api/comment_detail/{comment.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=comment.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], comment.id)

    def test_comment_detail_for_card_owner_view(self):
        list = List.objects.create(title='Test list', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test card', owner=self.user2, list=list)
        comment = Comment.objects.create(text='Test comment', author=self.user, card=card)
        request = self.factory.get(f'api/comment_detail/{comment.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=comment.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], comment.id)

    def test_comment_detail_notauthenticated_user_view(self):
        list = List.objects.create(title='Test list', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test card', owner=self.user, list=list)
        comment = Comment.objects.create(text='Test comment', author=self.user, card=card)
        request = self.factory.get(f'api/comment_detail/{comment.pk}/')
        response = self.view(request, pk=comment.pk)
        self.assertEqual(response.status_code, 403)

    def test_comment_detail_invalid_pk_view(self):
        list = List.objects.create(title='Test list', owner=self.user, board=self.board)
        card = Card.objects.create(title='Test card', owner=self.user2, list=list)
        comment = Comment.objects.create(text='Test comment', author=self.user, card=card)
        request = self.factory.get('api/comment_detail/999/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=999)
        self.assertEqual(response.status_code, 404)

class CommentDeleteViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = CommentDeleteAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(title='Test Board', user=self.user)
        self.list = List.objects.create(title='Test title', owner=self.user, board=self.board)
        self.card = Card.objects.create(title='Test title', owner=self.user, list=self.list)
        self.comment = Comment.objects.create(text='Test title', author=self.user2, card=self.card)

    def test_comment_delete_for_board_user_view(self):
        request = self.factory.delete(f'api/comment_delete/{self.comment.pk}/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=self.comment.pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Comment.objects.filter(pk=self.comment.pk).exists())

    def test_comment_delete_notrights_user_view(self):
        request = self.factory.delete(f'api/comment_delete/{self.comment.pk}/')
        force_authenticate(request, user=self.user)
        response = self.view(request, pk=self.comment.pk)
        self.assertEqual(response.status_code, 403)

    def test_comment_delete_invalid_pk_view(self):
        request = self.factory.delete('api/comment_delete/999/')
        force_authenticate(request, user=self.user2)
        response = self.view(request, pk=self.comment.pk)
        self.assertEqual(response.status_code, 204)

class CommentUpdateAPIViewTestCase(APITestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = CommentUpdateAPIView.as_view()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.user2 = User.objects.create_user(username='testuser2', password='testpassword2')
        self.board = Board.objects.create(
            title='Board 1',
            user=self.user,
        )
        self.list = List.objects.create(
            title='List 1',
            board=self.board,
            owner = self.user,
        )
        self.card = Card.objects.create(
            title='Card 1',
            list=self.list,
            description='description',
            end_date='2023-07-22',
            owner = self.user,
        )
        self.comment = Comment.objects.create(
            text = 'Test title comment',
            author = self.user2,
            card = self.card,
        )

    def test_comment_update_for_comment_author_view(self):
        self.list.owner = self.user
        self.card.owner = self.user
        url = reverse('comment_update', kwargs={'pk': self.comment.pk})
        data = {
            'text': 'Updated comment text',
        }
        self.client.force_authenticate(user=self.user2)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], self.comment.pk)
        self.assertEqual(response.data['text'], 'Updated comment text')

    def test_comment_update_for_notauthenticated_view(self):
        self.list.owner = self.user
        self.card.owner = self.user
        url = reverse('comment_update', kwargs={'pk': self.comment.pk})
        data = {
            'text': 'Updated comment text',
        }
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_comment_update_for_no_comment_author_view(self):
        self.list.owner = self.user
        self.card.owner = self.user
        url = reverse('comment_update', kwargs={'pk': self.comment.pk})
        data = {
            'text': 'Updated comment text',
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_comment_update_invalid_pk_view(self):
        self.list.owner = self.user
        self.card.owner = self.user
        url = reverse('comment_update', kwargs={'pk': 999})
        data = {
            'text': 'Updated comment text',
        }
        self.client.force_authenticate(user=self.user2)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 404)
