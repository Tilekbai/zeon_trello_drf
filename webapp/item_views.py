from rest_framework.response import (
    Response,
)
from rest_framework import (
    generics,
) 
from rest_framework.views import (
    APIView,
    )
from rest_framework import (
    permissions,
    )
from rest_framework.views import APIView
from rest_framework import status
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from django.http import Http404
from .models import (
    List,
    Checklist,
    Card,
    Board,
    Item,
)
from .serializers import (
    ItemSerializer,
)

User = get_user_model()   

class ItemCreateAPIView(generics.CreateAPIView):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        checklist_id = self.kwargs.get('pk')
        checklist = Checklist.objects.get(pk=checklist_id)
        card = Card.objects.get(pk=checklist.card.pk)
        list = List.objects.get(pk=card.list.pk)
        board = Board.objects.get(pk=list.board.pk)
        if checklist.owner == self.request.user or self.request.user == card.owner or list.owner == self.request.user or board.user == self.request.user or self.request.user in card.members.all() or self.request.user in board.members.all():
            if serializer.is_valid():
                serializer.save(owner=self.request.user, checklist=checklist)
                return Response(status=201)
            return Response(status=400)
        else:
            return Response(status=403)

class ItemDetailAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, pk, format=None):
        item = get_object_or_404(Item, pk=pk)
        checklist = Checklist.objects.get(pk=item.checklist.pk)
        card = Card.objects.get(id=checklist.card.pk)
        list = List.objects.get(id=card.list.pk)
        board = Board.objects.get(id=list.board.pk)
        if item.owner == self.request.user or checklist.owner == self.request.user or card.owner == self.request.user or self.request.user in card.members.all() or list.owner == self.request.user or self.request.user == board.user or self.request.user in board.members.all():
            serializer = ItemSerializer(item)
            return Response(serializer.data)
        else:
            raise Http404('Page not found')

class ItemDeleteAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def delete(self, request, pk, format=None):
        try:
            item = Item.objects.get(pk=pk)
            checklist = Checklist.objects.get(pk=item.checklist.pk)
            card = Card.objects.get(pk=checklist.card.pk)
            list = List.objects.get(pk=card.list.id)
            board = card.list.board
            if item.owner == self.request.user or self.request.user == checklist.owner or board.user == self.request.user or self.request.user == list.owner or card.owner == self.request.user or self.request.user in card.members.all():
                list.delete()
            else:
                return Response(status=403)
        except Item.DoesNotExist:
            return Response({'detail': 'Card not found.'}, status=404)
        return Response(status=204)

class ItemListAPIView(generics.ListAPIView):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer

    def get(self, request, *args, **kwargs):
        checklist = Checklist.objects.get(pk=kwargs['pk'])
        queryset = self.get_queryset().filter(checklist=checklist)
        card = Card.objects.get(pk=checklist.card.pk)
        list = List.objects.get(pk=card.list.pk)
        board = Board.objects.get(pk=list.board.pk)
        if checklist.owner == self.request.user or card.owner == self.request.user or self.request.user in card.members.all() or list.owner == self.request.user or board.user == self.request.user or self.request.user in board.members.all():
            serializer = self.get_serializer(queryset, many=True)
            context = {'items': serializer.data}
            return Response(context)
        else:
            raise Http404("Page not found")

class ItemUpdateAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    def put(self, request, pk, format=None):
        item = get_object_or_404(Item, pk=pk)
        checklist = Checklist.objects.get(pk=item.checklist.pk)
        card = Card.objects.get(pk=checklist.card.pk)
        if item.owner == self.request.user or checklist.owner == self.request.user or card.owner == self.request.user or self.request.user in card.members.all() or card.list.board.user == self.request.user or card.list.owner == self.request.user or self.request.user in card.list.board.members.all():
            serializer = ItemSerializer(item, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status=400)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)
