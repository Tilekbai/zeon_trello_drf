from django.db import models
from django.contrib.auth import get_user_model
from django.urls import reverse


COLOUR_CHOICES = (
    ("white", "White"),
    ("blue", "Blue"),
    ("yellow", "Yellow"),
    ("green", "Green"),
    ("grey", "Grey"),
    ("violet", "Violet"),
    ("orange", "Orange"),
    ("red", "Red"),
    ("other", "Other"),
)

class Board(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, blank=False, related_name="own_boards", null=True)
    title = models.CharField(max_length=77, null=False, blank=False)
    background = models.ImageField()
    favorite = models.BooleanField(default=False)
    archive = models.BooleanField(default=False)
    members = models.ManyToManyField(get_user_model(), blank=True, related_name="member_boards")

    def __str__(self):
        return f"{self.title}. User: {self.user}. Favorite: {self.favorite}. Archive: {self.archive}. Members: {self.members}"
    
class List(models.Model):
    owner = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, blank=False, related_name="own_lists", null=True)
    title = models.CharField(max_length=30, null=True, blank=False)
    board = models.ForeignKey(Board, on_delete=models.CASCADE, blank=False, null=False, related_name="boards")

    def __str__(self):
        return f"Title: {self.title}. Board: {self.board.title}. Owner: {self.owner}"
    
    def get_absolute_url(self):
        return reverse('list_detail', args=(self.id,))
    
class Card(models.Model):
    owner = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, blank=False, related_name="own_cards", null=True)
    title = models.CharField(max_length=30, null=True, blank=False)
    list = models.ForeignKey(List, on_delete=models.CASCADE, null=False, blank=False, related_name="cards")
    description = models.TextField(max_length=500, blank=True, null=True)
    members = models.ManyToManyField(get_user_model(), blank=False, related_name="card_members")
    end_date = models.DateField(null=True, blank=True)
    attachment = models.FileField(upload_to="uploads/", blank=True, null=True)

    def get_absolute_url(self):
        return reverse('card_detail', args=(self.id,))
    
    def __str__(self):
        return f"{self.title}. List: {self.list.title}, owner: {self.owner.username}."

class Comment(models.Model):
    card = models.ForeignKey(Card, on_delete=models.CASCADE, blank=False, null=False, related_name="comments")
    text = models.TextField(max_length=300, blank=True, null=True)
    author = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, null=False, blank=False, related_name="comments")
    created_at = models.DateField(auto_now_add=True)

    def get_absolute_url(self):
        return reverse('comment_detail', args=(self.id,))
    
    def __str__(self):
        return f"{self.card}. {self.author}"

class Checklist(models.Model):
    owner = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, blank=False, related_name="own_checklists", null=True)
    card = models.ForeignKey(Card, on_delete=models.CASCADE, blank=False, null=False, related_name="checklists")
    title = models.CharField(max_length=50, blank=False, null=False)

    def get_absolute_url(self):
        return reverse('checklist_detail', args=(self.id,)) 
    
    def __str__(self):
        return f"{self.title}"
    
class Item(models.Model):
    owner = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, blank=False, related_name="own_items", null=True)
    title = models.CharField(max_length=500, blank=False, null=False)
    checklist = models.ForeignKey(Checklist, on_delete=models.CASCADE, blank=False, null=True, related_name="items")
    assign = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, blank=True, null=True, related_name="items")

    def get_absolute_url(self):
        return reverse('item_detail', args=(self.id,)) 
    
    def __str__(self):
        return f"{self.title}"

class Label(models.Model):
    owner = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, blank=False, related_name="own_labels", null=True)
    card = models.ForeignKey(Card, on_delete=models.CASCADE, blank=False, related_name="labels")
    title = models.CharField(max_length=30, blank=False, null=False)
    color = models.CharField(max_length = 20, choices = COLOUR_CHOICES, default = 'green')
    custom_color = models.CharField(max_length=20, blank=True)

    def save(self, *args, **kwargs):
        if self.color != 'other':
            self.custom_color = ''
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('label_detail', args=(self.id,))
    
    def __str__(self):
        return f"{self.title}. Card: {self.card}. Color: {self.color}"