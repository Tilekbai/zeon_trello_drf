from django.contrib import admin
from .models import (
    Board,
    List,
    Card,
    Comment,
    Item,
    Checklist,
    Label,
)


class BoardAdmin(admin.ModelAdmin):
    fields = ['title', 'user', 'background', 'members', 'favorite', 'archive']

admin.site.register(Board, BoardAdmin)

class ListAdmin(admin.ModelAdmin):
    fields = ['title', 'board', 'owner']

admin.site.register(List, ListAdmin)

class CardAdmin(admin.ModelAdmin):
    fields = ['title', 'owner', 'list', 'members', 'end_date']

admin.site.register(Card, CardAdmin)

class ChecklistAdmin(admin.ModelAdmin):
    fields = ['owner', 'title', 'card']

admin.site.register(Checklist, ChecklistAdmin)

class CommentAdmin(admin.ModelAdmin):
    fields = ['text', 'author', 'card']

admin.site.register(Comment, CommentAdmin)

class ItemAdmin(admin.ModelAdmin):
    fields = ['title', 'assign']

admin.site.register(Item, ItemAdmin)

class LabelAdmin(admin.ModelAdmin):
    fields = ['title', 'card', 'color']

admin.site.register(Label, LabelAdmin)