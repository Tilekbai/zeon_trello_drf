from rest_framework.response import (
    Response,
)
from rest_framework import (
    generics,
) 
from rest_framework.views import (
    APIView,
    )
from rest_framework import (
    permissions,
    )
from rest_framework.views import APIView
from rest_framework import status
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from django.http import Http404
from .models import (
    List,
    Checklist,
    Card,
    Board,
    Label,
)
from .serializers import (
    LabelSerializer,
)

User = get_user_model()   

class LabelCreateAPIView(generics.CreateAPIView):
    queryset = Label.objects.all()
    serializer_class = LabelSerializer
    permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        card = Card.objects.get(pk=self.kwargs['pk'])
        list = List.objects.get(pk=card.list.pk)
        board = Board.objects.get(pk=list.board.pk)
        if self.request.user == card.owner or list.owner == self.request.user or board.user == self.request.user or self.request.user in card.members.all() or self.request.user in board.members.all():
            if serializer.is_valid():
                serializer.save(owner=self.request.user, card=card)
                return Response(status=201)
            return Response(status=400)
        else:
            return Response(status=403)

class LabelDetailAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, pk, format=None):
        label = get_object_or_404(Label, pk=pk)
        card = Card.objects.get(id=label.card.pk)
        list = List.objects.get(pk=card.list.id)
        board = Board.objects.get(pk=list.board.id)
        if label.owner == self.request.user or card.owner == self.request.user or self.request.user in card.members.all() or self.request.user == board.user or list.owner == self.request.user:
            serializer = LabelSerializer(label)
            return Response(serializer.data)
        else:
            raise Http404('Page not found')

class LabelDeleteAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def delete(self, request, pk, format=None):
        try:
            label = Label.objects.get(pk=pk)
            if label.owner == self.request.user:
                label.delete()
            else:
                return Response(status=403)
        except Label.DoesNotExist:
            return Response({'detail': 'Label not found.'}, status=404)
        return Response(status=204)

class LabelUpdateAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    def put(self, request, pk, format=None):
        label = get_object_or_404(Label, pk=pk)
        card = Card.objects.get(pk=label.card.pk)
        if label.owner == self.request.user:
            serializer = LabelSerializer(label, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status=400)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)
