Project Deployment Instructions

Инструкция по развертыванию проекта
Склонируйте репозиторий из Gitlab на свой локальный компьютер с помощью команды:
git clone https://gitlab.com/Tilekbai/zeon_trello_drf.git
Перейдите в корневой каталог проекта.
Создайте виртуальное окружение, выполнив команду:
python3 -m venv venv
Активируйте виртуальное окружение, выполнив команду:
source venv/bin/activate
Создайте файл - .env:
POSTGRES_HOST=db
POSTGRES_PORT=5432
POSTGRES_NAME=***
POSTGRES_USER=***
POSTGRES_PASSWORD=***
Создайте файл - users/config.py
Внутрь файла добавьте следующие настройки:
EMAIL_HOSTUSER=***********
EMAIL_HOSTPASSWORD=*******

POSTGRES_HOST_TEST=localhost
POSTGRES_PORT_TEST=5432
POSTGRES_NAME_TEST=*****
POSTGRES_USER_TEST=******
POSTGRES_PASSWORD_TEST=****
В корневой директории вашего проекта, где находится Dockerfile. Выполните команду:

sudo docker build -t trello ., чтобы собрать Docker-образ

Запустите контейнеры: После успешной сборки образа выполните команду:

docker-compose up, чтобы запустить контейнеры